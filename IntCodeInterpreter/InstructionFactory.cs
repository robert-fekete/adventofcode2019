﻿using AdventOfCode2019.IntCode.Program;
using AdventOfCode2019.IntCode.Program.Instructions;
using AdventOfCode2019.IntCode.Program.IO;
using System;

namespace AdventOfCode2019.IntCode
{
    internal class InstructionFactory
    {
        private readonly RelativeBase relativeBase = new RelativeBase();
        private readonly Cursor cursor;
        private readonly InstructionPointer ip;

        public InstructionFactory(Cursor cursor, InstructionPointer ip)
        {
            this.cursor = cursor;
            this.ip = ip;
        }

        public IInstruction Create(long instructionCode, StandardInput input, StandardOutput output)
        {
            var parameters = new ParameterReader((int)instructionCode / 100, cursor, relativeBase);
            var opcode = instructionCode % 100;
            switch (opcode)
            {
                case 1:
                    return new Addition(parameters);
                case 2:
                    return new Multiplication(parameters);
                case 3:
                    return new ReadInput(parameters, input);
                case 4:
                    return new WriteOutput(parameters, output);
                case 5:
                    return new Jump(parameters, ip, i => i != 0);
                case 6:
                    return new Jump(parameters, ip, i => i == 0);
                case 7:
                    return new Compare(parameters, (i1, i2) => i1 < i2);
                case 8:
                    return new Compare(parameters, (i1, i2) => i1 == i2);
                case 9:
                    return new ModifyRelativeBase(parameters, relativeBase);
                default:
                    throw new InvalidOperationException($"Invalid opcode '{opcode}'");
            }
        }
    }
}