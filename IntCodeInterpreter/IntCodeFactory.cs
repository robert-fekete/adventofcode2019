﻿using AdventOfCode2019.IntCode.Program;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.IntCode
{
    public class IntCodeFactory
    {
        public IIntCodeComputer Create(IEnumerable<string> code)
        {
            var memory = new Memory(code.Select(long.Parse));
            var ip = new InstructionPointer();
            return new IntCodeComputer(memory, ip);
        }
    }
}
