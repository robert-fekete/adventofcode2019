﻿using System;

namespace AdventOfCode2019.IntCode.Program.Parameters
{
    internal class ImmediateParameter : IParameter
    {
        private readonly Cursor cursor;
        private readonly long offset;

        public ImmediateParameter(Cursor cursor, long offset)
        {
            this.cursor = cursor;
            this.offset = offset;
        }

        public long Value 
        {
            get
            {
                return cursor.GetValue(offset);
            }
            set
            {
                throw new InvalidOperationException("Immediate parameters are read-only");
            }
        }
    }
}