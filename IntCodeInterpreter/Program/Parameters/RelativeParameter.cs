﻿namespace AdventOfCode2019.IntCode.Program.Parameters
{
    internal class RelativeParameter : IParameter
    {
        private readonly Cursor cursor;
        private readonly long relativeBase;
        private readonly long offset;

        public RelativeParameter(Cursor cursor, long relativeBase, long offset)
        {
            this.cursor = cursor;
            this.relativeBase = relativeBase;
            this.offset = offset;
        }

        public long Value
        {
            get
            {
                var position = cursor.GetValue(offset) + relativeBase;
                return cursor.GetAbsolutePosition(position);
            }
            set
            {
                var position = cursor.GetValue(offset) + relativeBase;
                cursor.SetAbsolutePosition(position, value);
            }
        }
    }
}
