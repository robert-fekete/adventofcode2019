﻿namespace AdventOfCode2019.IntCode.Program.Parameters
{
    internal class PositionalParameter : IParameter
    {
        private readonly Cursor cursor;
        private readonly long offset;

        public PositionalParameter(Cursor cursor, long offset)
        {
            this.cursor = cursor;
            this.offset = offset;
        }

        public long Value
        {
            get
            {
                var position = cursor.GetValue(offset);
                return cursor.GetAbsolutePosition(position);
            }
            set
            {
                var position = cursor.GetValue(offset);
                cursor.SetAbsolutePosition(position, value);
            }
        }
    }
}