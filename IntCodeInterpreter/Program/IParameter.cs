﻿namespace AdventOfCode2019.IntCode.Program
{
    internal interface IParameter
    {
        long Value { get; set; }
    }
}
