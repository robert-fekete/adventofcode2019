﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2019.IntCode.Program.IO
{
    public class StandardOutput
    {
        private readonly List<long> outputs = new List<long>();

        public void Write(long value)
        {
            outputs.Add(value);
        }

        public IEnumerable<long> Dump()
        {
            return outputs;
        }
    }
}
