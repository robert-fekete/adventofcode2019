﻿using System;
using System.Runtime.Serialization;

namespace AdventOfCode2019.IntCode.Program.IO
{
    [Serializable]
    internal class InvalidStandardInputReadException : Exception
    {
        public InvalidStandardInputReadException()
        {
        }

        public InvalidStandardInputReadException(string message) : base(message)
        {
        }

        public InvalidStandardInputReadException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidStandardInputReadException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}