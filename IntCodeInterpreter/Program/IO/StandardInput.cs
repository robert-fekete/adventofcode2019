﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.IntCode.Program.IO
{
    internal class StandardInput
    {
        private readonly long[] input;
        private int index = 0;

        public StandardInput(IEnumerable<long> input)
        {
            this.input = input.ToArray();
        }

        public bool IsEmpty => index == input.Length;

        public long Read()
        {
            if (index >= input.Length)
            {
                throw new InvalidStandardInputReadException("More values are read from the input than what is there");
            }
            return input[index++];
        }
    }
}
