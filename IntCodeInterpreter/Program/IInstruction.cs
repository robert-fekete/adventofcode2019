﻿namespace AdventOfCode2019.IntCode.Program
{
    internal interface IInstruction
    {
        int Offset { get; }

        void Execute(ref bool isPaused);
    }
}