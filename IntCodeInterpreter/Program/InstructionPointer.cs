﻿using System;

namespace AdventOfCode2019.IntCode.Program
{
    internal class InstructionPointer
    {
        public long Value { get; private set; } = 0;

        internal void Move(long offset)
        {
            Value += offset;
        }

        internal void JumpTo(long address)
        {
            Value = address;
        }
    }
}