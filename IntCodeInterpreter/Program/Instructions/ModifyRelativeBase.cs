﻿namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class ModifyRelativeBase : IInstruction
    {
        private readonly ParameterReader parameters;
        private readonly RelativeBase relativeBase;

        public ModifyRelativeBase(ParameterReader parameters, RelativeBase relativeBase)
        {
            this.parameters = parameters;
            this.relativeBase = relativeBase;
        }

        public int Offset => 2;

        public void Execute(ref bool _)
        {
            var a = parameters.GetParameter(0);
            relativeBase.Value += a.Value;
        }
    }
}