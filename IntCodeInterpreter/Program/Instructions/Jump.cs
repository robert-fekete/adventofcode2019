﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class Jump : IInstruction
    {
        private const int INSTRUCTION_OFFSET = 3;
        private readonly ParameterReader parameters;
        private readonly InstructionPointer ip;
        private readonly Predicate<long> predicate;

        public Jump(ParameterReader parameters, InstructionPointer ip, Predicate<long> predicate)
        {
            this.parameters = parameters;
            this.ip = ip;
            this.predicate = predicate;
        }

        public int Offset { get; private set; } = INSTRUCTION_OFFSET;

        public void Execute(ref bool _)
        {
            var a = parameters.GetParameter(0);
            var b = parameters.GetParameter(1);

            if (predicate(a.Value))
            {
                ip.JumpTo(b.Value);
                Offset = 0;
            }
        }
    }
}
