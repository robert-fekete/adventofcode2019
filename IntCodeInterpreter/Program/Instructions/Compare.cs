﻿using System;

namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class Compare : IInstruction
    {
        private readonly ParameterReader parameters;
        private readonly Func<long, long, bool> compare;

        public Compare(ParameterReader parameters, Func<long, long, bool> compare)
        {
            this.parameters = parameters;
            this.compare = compare;
        }

        public int Offset => 4;

        public void Execute(ref bool _)
        {
            var a = parameters.GetParameter(0);
            var b = parameters.GetParameter(1);
            var c = parameters.GetParameter(2);
            if (compare(a.Value, b.Value))
            {
                c.Value = 1;
            }
            else
            {
                c.Value = 0;
            }
        }
    }
}
