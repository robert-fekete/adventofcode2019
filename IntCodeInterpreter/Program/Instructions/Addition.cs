﻿namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class Addition : IInstruction
    {
        private readonly ParameterReader parameters;

        public Addition(ParameterReader parameters)
        {
            this.parameters = parameters;
        }

        public int Offset => 4;

        public void Execute(ref bool _)
        {
            var a = parameters.GetParameter(0);
            var b = parameters.GetParameter(1);
            var c = parameters.GetParameter(2);
            c.Value = a.Value + b.Value;
        }
    }
}
