﻿using AdventOfCode2019.IntCode.Program.IO;

namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class WriteOutput : IInstruction
    {
        private readonly ParameterReader parameters;
        private readonly StandardOutput output;

        public WriteOutput(ParameterReader parameters, StandardOutput output)
        {
            this.parameters = parameters;
            this.output = output;
        }

        public int Offset => 2;

        public void Execute(ref bool _)
        {
            var a = parameters.GetParameter(0);
            output.Write(a.Value);
        }
    }
}
