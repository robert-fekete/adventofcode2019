﻿using AdventOfCode2019.IntCode.Program.IO;

namespace AdventOfCode2019.IntCode.Program.Instructions
{
    internal class ReadInput : IInstruction
    {
        private readonly ParameterReader parameters;
        private readonly StandardInput input;

        public ReadInput(ParameterReader parameters, StandardInput input)
        {
            this.parameters = parameters;
            this.input = input;
        }

        public int Offset => 2;

        public void Execute(ref bool isPaused)
        {
            var a = parameters.GetParameter(0);
            if (input.IsEmpty)
            {
                isPaused = true;
                return;
            }
            
            a.Value = input.Read();
            isPaused = false;
        }
    }
}
