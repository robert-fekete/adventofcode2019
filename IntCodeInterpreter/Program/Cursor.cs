﻿using System;

namespace AdventOfCode2019.IntCode.Program
{
    internal class Cursor
    {
        private readonly InstructionPointer ip;
        private readonly Memory memory;

        public Cursor(InstructionPointer ip, Memory memory)
        {
            this.ip = ip;
            this.memory = memory;
        }

        public long CurrentValue => memory.GetValue(ip.Value);

        public void MovePointer(long offset)
        {
            ip.Move(offset);
        }

        public long GetAbsolutePosition(long index)
        {
            return memory.GetValue(index);
        }

        public void SetAbsolutePosition(long index, long value)
        {
            memory.SetValue(index, value);
        }

        public long GetValue(long offset)
        {
            return memory.GetValue(ip.Value + offset);
        }
    }
}
