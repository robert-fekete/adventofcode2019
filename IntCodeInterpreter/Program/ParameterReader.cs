﻿using AdventOfCode2019.IntCode.Program.Parameters;
using System;
using System.Collections.Generic;

namespace AdventOfCode2019.IntCode.Program
{
    internal class ParameterReader
    {
        private readonly int rawModes;
        private readonly List<ParameterMode> modes = new List<ParameterMode>();
        private readonly Cursor cursor;
        private readonly RelativeBase relativeBase;

        public ParameterReader(int rawModes, Cursor cursor, RelativeBase relativeBase)
        {
            this.rawModes = rawModes;
            this.cursor = cursor;
            this.relativeBase = relativeBase;
            ParseParameterModes();
        }

        internal IParameter GetParameter(int index)
        {
            var mode = GetMode(index);
            switch (mode)
            {
                case ParameterMode.Position:
                    return new PositionalParameter(cursor, index + 1);  // +1 due to the opcode
                case ParameterMode.Immediate:
                    return new ImmediateParameter(cursor, index + 1); // +1 due to the opcode
                case ParameterMode.Relative:
                    return new RelativeParameter(cursor, relativeBase.Value, index + 1); // +1 due to the opcode
                default:
                    throw new InvalidOperationException($"Invalid parameter mode {(int)mode}");
            }
        }

        private ParameterMode GetMode(int index)
        {
            if (index >= modes.Count)
            {
                return ParameterMode.Position;
            }

            return modes[index];
        }

        private void ParseParameterModes()
        {
            var current = rawModes;
            while (current != 0)
            {
                var mode = current % 10;
                modes.Add((ParameterMode)mode);
                current /= 10;
            }
        }

        private enum ParameterMode
        {
            Position = 0,
            Immediate = 1,
            Relative = 2
        }
    }
}
