﻿namespace AdventOfCode2019.IntCode.Program
{
    internal class RelativeBase
    {
        public long Value { get; set; } = 0;
    }
}
