﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.IntCode.Program
{
    internal class Memory
    {
        private long[] values;
        private readonly Dictionary<long, long> tailValues = new Dictionary<long, long>();

        public Memory(IEnumerable<long> initialValues)
        {
            values = initialValues.ToArray();
        }

        public int Length => values.Length;

        internal long GetValue(long index)
        {
            if (index < 0)
            {
                throw new InvalidOperationException($"Memory corruption. Address {index} is below 0.");
            }
            if (index >= Length)
            {
                return tailValues.ContainsKey(index) ? tailValues[index] : 0;
            }

            return values[index];
        }

        internal void SetValue(long index, long value)
        {
            if (index < 0)
            {
                throw new InvalidOperationException($"Memory corruption. Address {index} is below 0.");
            }
            if (index >= Length)
            {
                tailValues[index] = value;
            }
            else
            {
                values[index] = value;
            }
        }
    }
}