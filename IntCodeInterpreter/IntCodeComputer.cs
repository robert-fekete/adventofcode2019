﻿using AdventOfCode2019.IntCode.Program;
using AdventOfCode2019.IntCode.Program.IO;
using System.Collections.Generic;

namespace AdventOfCode2019.IntCode
{
    internal class IntCodeComputer : IIntCodeComputer
    {
        private const int END_CODE = 99;
        private readonly InstructionFactory instructionFactory;
        private readonly Memory memory;
        private readonly Cursor cursor;

        public IntCodeComputer(Memory memory, InstructionPointer ip)
        {
            this.memory = memory;
            cursor = new Cursor(ip, memory);
            instructionFactory = new InstructionFactory(cursor, ip);
        }

        public bool Finished => cursor.CurrentValue == END_CODE;

        public IEnumerable<long> Execute(IEnumerable<long> rawInput)
        {
            var output = new StandardOutput();
            var input = new StandardInput(rawInput);

            var isPaused = false;
            while (!Finished)
            {
                var opcode = cursor.CurrentValue;
                var instruction = instructionFactory.Create(opcode, input, output);

                instruction.Execute(ref isPaused);
                if (isPaused)
                {
                    break;
                }

                cursor.MovePointer(instruction.Offset);
            }

            return output.Dump();
        }

        public long GetResult(int index)
        {
            return memory.GetValue(index);
        }
    }
}
