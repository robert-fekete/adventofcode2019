﻿using AdventOfCode2019.IntCode.Program;
using AdventOfCode2019.IntCode.Program.IO;
using System.Collections.Generic;

namespace AdventOfCode2019.IntCode
{
    public interface IIntCodeComputer
    {
        bool Finished { get; }

        IEnumerable<long> Execute(IEnumerable<long> input);
        long GetResult(int index);
    }
}