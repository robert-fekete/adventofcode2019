﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class MapParser
    {
        public MapFactory ParseFirst(IEnumerable<string> input)
        {
            var nodes = ParseNodes(input);

            return new MapFactory(nodes, nodes.Values.Where(n => n.IsEntrance));
        }

        public MapFactory ParseSecond(IEnumerable<string> input)
        {
            var nodes = new Dictionary<(int, int), Node>();

            var map = input.ToArray();
            for (int y = 0; y < map.Length; y++)
            {
                for (int x = 0; x < map[y].Length; x++)
                {
                    if (map[y][x] != '#')
                    {
                        var node = new Node((x, y), map[y][x]);
                        nodes[(x, y)] = node;
                    }
                }
            }

            var entrance = nodes.Single(kvp => kvp.Value.IsEntrance).Key;
            nodes.Remove(entrance);
            nodes.Remove((entrance.Item1 + 1, entrance.Item2));
            nodes.Remove((entrance.Item1 - 1, entrance.Item2));
            nodes.Remove((entrance.Item1, entrance.Item2 + 1));
            nodes.Remove((entrance.Item1, entrance.Item2 - 1));
            nodes.Remove((entrance.Item1 + 1, entrance.Item2 + 1));
            nodes.Remove((entrance.Item1 + 1, entrance.Item2 - 1));
            nodes.Remove((entrance.Item1 - 1, entrance.Item2 + 1));
            nodes.Remove((entrance.Item1 - 1, entrance.Item2 - 1));

            AddNode(nodes, (entrance.Item1 + 1, entrance.Item2 + 1));
            AddNode(nodes, (entrance.Item1 + 1, entrance.Item2 - 1));
            AddNode(nodes, (entrance.Item1 - 1, entrance.Item2 + 1));
            AddNode(nodes, (entrance.Item1 - 1, entrance.Item2 - 1));

            return new MapFactory(nodes, nodes.Values.Where(n => n.IsEntrance));
        }

        private void AddNode(Dictionary<(int, int), Node> nodes, (int, int) position)
        {
            nodes.Add(position, new Node(position, '@'));
        }

        private static Dictionary<(int, int), Node> ParseNodes(IEnumerable<string> input)
        {
            var nodes = new Dictionary<(int, int), Node>();

            var map = input.ToArray();
            for (int y = 0; y < map.Length; y++)
            {
                for (int x = 0; x < map[y].Length; x++)
                {
                    if (map[y][x] != '#')
                    {
                        var node = new Node((x, y), map[y][x]);
                        nodes[(x, y)] = node;
                    }
                }
            }

            return nodes;
        }
    }
}
