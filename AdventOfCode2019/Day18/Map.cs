﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class Map
    {
        private readonly Graph graph;

        private readonly Dictionary<char, IEnumerable<char>> keyDependencies;
        private readonly Dictionary<(int, int), Dictionary<(int, int), int>> distanceMatrix;

        public Map(Graph graph)
        {
            this.graph = graph;
            keyDependencies = GetKeyDependencies();
            distanceMatrix = GetDistanceMatrix();
        }

        public bool HasAllKeys(IEnumerable<char> collectedKeys)
        {
            return keyDependencies.Keys.All(k => collectedKeys.Contains(k));
        }

        public IReadOnlyCollection<char> GetAccessibleKeys(IEnumerable<char> collectedKeys)
        {
            var accessibleKeys = keyDependencies.Keys
                                                .Where(k => !collectedKeys.Contains(k))
                                                .Where(k => keyDependencies[k].All(d => collectedKeys.Contains(d)))
                                                .ToArray();

            return accessibleKeys;
        }

        public int GetDistance(char sourceKey, char targetKey)
        {
            var sourceNode = graph.GetKeyPosition(sourceKey);
            var targetNode = graph.GetKeyPosition(targetKey);
            var distance = distanceMatrix[sourceNode][targetNode];

            if (distance == int.MaxValue)
            {
                throw new InvalidOperationException();
            }
            return distance;
        }

        private Dictionary<(int, int), Dictionary<(int, int), int>> GetDistanceMatrix()
        {
            var matrix = CreateInitialMatrix();
            foreach(var c in graph.Nodes.Select(n => n.Position))
            {
                foreach(var a in graph.Nodes.Select(n => n.Position))
                {
                    if (matrix[a][c] == int.MaxValue)
                    {
                        continue;
                    }
                    foreach (var b in graph.Nodes.Select(n => n.Position))
                    {
                        if (matrix[c][b] == int.MaxValue)
                        {
                            continue;
                        }

                        if (matrix[a][b] > matrix[a][c] + matrix[c][b])
                        {
                            matrix[a][b] = matrix[a][c] + matrix[c][b];
                        }
                    }
                }
            }

            return matrix;
        }

        private Dictionary<(int, int), Dictionary<(int, int), int>> CreateInitialMatrix()
        {
            var matrix = new Dictionary<(int, int), Dictionary<(int, int), int>>();

            foreach (var node1 in graph.Nodes)
            {
                matrix[node1.Position] = new Dictionary<(int, int), int>();
                foreach (var node2 in graph.Nodes)
                {
                    if (node1.Position == node2.Position)
                    {
                        matrix[node1.Position][node2.Position] = 0;
                    }
                    else
                    {
                        matrix[node1.Position][node2.Position] = graph.GetWeight(node1, node2);
                    }
                }
            }

            return matrix;
        }

        private Dictionary<char, IEnumerable<char>> GetKeyDependencies()
        {
            var dependencies = new Dictionary<char, IEnumerable<char>>();
            var bfs = new BfsTraversal<IEnumerable<char>>(new char[0], 
                                                          (key, requiredKeys) =>
                                                          {
                                                              if (key.IsKey)
                                                              {
                                                                  dependencies[key.Type] = requiredKeys;
                                                              }
                                                          },
                                                          (doors, edge) =>
                                                          {
                                                              var newDoors = new List<char>(doors);
                                                              newDoors.AddRange(edge.Doors);
                                                              return newDoors;
                                                          });

            graph.Traverse(bfs);

            return dependencies;
        }
    }
}
