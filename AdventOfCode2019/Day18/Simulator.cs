﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class Simulator
    {
        private readonly IList<Explorer> explorers;

        public Simulator(IEnumerable<Explorer> explorers)
        {
            this.explorers = explorers.ToArray();
        }

        public int GetShortestCollection()
        {
            var result = GetShortestCollectionRec(explorers, new char[0]).Sum();
            return result;
        }

        private IReadOnlyCollection<int> GetShortestCollectionRec(IList<Explorer> explorers, IEnumerable<char> collectedKeys)
        {
            if (explorers.All(e => e.HasState(collectedKeys)))
            {
                return explorers.Select(e => e.RestoreState(collectedKeys).TotalDistance).ToArray(); // Converting this to an array gives 10-15% performance boost. What do you know...
            }

            var bestDistances = GetShortestCollectionCalc(explorers, collectedKeys);

            foreach((var explorer, var bestDistance) in explorers.Zip(bestDistances, (a, b) => (a, b)))
            {
                explorer.SaveState(bestDistance, collectedKeys);
            }

            return bestDistances;
        }

        private IReadOnlyCollection<int> GetShortestCollectionCalc(IList<Explorer> explorers, IEnumerable<char> collectedKeys)
        {
            if (explorers.All(e => e.IsFinished(collectedKeys)))
            {
                return explorers.Select(e => e.TotalDistance).ToArray();
            }

            var bestDistances = Enumerable.Empty<int>();
            var shortestDistance = int.MaxValue;
            for(int i = 0; i < explorers.Count; i++)
            {
                var explorer = explorers[i];
                var keysToCollect = explorer.GetNextKeys(collectedKeys);
                foreach (var key in keysToCollect)
                {
                    var nextExplorers = explorers.ToArray();
                    nextExplorers[i] = explorer.Move(key);
                    
                    var nextKeys = new List<char>(collectedKeys);
                    nextKeys.Add(key);

                    var distances = GetShortestCollectionRec(nextExplorers, nextKeys);
                    var totalDistance = distances.Sum();
                    if (totalDistance < shortestDistance)
                    {
                        shortestDistance = totalDistance;
                        bestDistances = distances;
                    }
                }
            }

            return bestDistances.ToArray();
        }
    }
}
