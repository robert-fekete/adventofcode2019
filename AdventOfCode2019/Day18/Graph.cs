﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day18
{
    internal class Graph
    {
        private readonly Node root;
        private Dictionary<Node, Dictionary<Node, Edge>> edges;
        private readonly Dictionary<char, (int, int)> keyMapping;

        public Graph(Node root, Dictionary<Node, Dictionary<Node, Edge>> edges, Dictionary<char, (int, int)> keyMapping)
        {
            this.root = root;
            this.edges = edges;
            this.keyMapping = keyMapping;
        }

        public IEnumerable<Node> Nodes => edges.Keys;

        public (int, int) GetKeyPosition(char key)
        {
            return keyMapping[key];
        }

        public IEnumerable<Node> GetNeighbours(Node node)
        {
            return edges[node].Keys;
        }

        public int GetWeight(Node node1, Node node2)
        {
            if (edges[node1].ContainsKey(node2))
            {
                return edges[node1][node2].Weight;
            }

            return int.MaxValue;
        }

        public void Traverse<T>(BfsTraversal<T> traversal)
        {
            traversal.Traverse(root, edges);
        }
    }
}