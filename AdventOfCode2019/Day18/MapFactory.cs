﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class MapFactory
    {
        private readonly (int Dx, int Dy)[] offsets = new[]
        {
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0)
        };
        private readonly Dictionary<(int, int), Node> nodes;
        private readonly IEnumerable<Node> entrances;

        public MapFactory(Dictionary<(int, int), Node> nodes, IEnumerable<Node> entrances)
        {
            this.nodes = nodes;
            this.entrances = entrances;
        }


        public IEnumerable<Map> CreateMaps()
        {
            var maps = new List<Map>();
            foreach(var entrance in entrances)
            {
                var graph = Traverse(entrance);
                maps.Add(new Map(graph));
            }

            return maps;
        }

        private Graph Traverse(Node sourceNode)
        {
            var keyMapping = new Dictionary<char, (int, int)>();
            var edges = new Dictionary<Node, Dictionary<Node, Edge>>();
            var backlog = new Queue<(Node CurrentNode, Node ParentNode, int Distance, IEnumerable<char> Doors)>();
            var visited = new HashSet<Node>();

            backlog.Enqueue((sourceNode, sourceNode, 0, new char[0]));
            while (backlog.Count > 0)
            {
                (var currentNode, var parentNode, var distance, var doors) = backlog.Dequeue();

                if (visited.Contains(currentNode))
                {
                    continue;
                }
                visited.Add(currentNode);

                if (currentNode.IsKey)
                {
                    keyMapping[currentNode.Type] = currentNode.Position;
                }


                var neighbours = GetNeighbours(currentNode.Position);
                if (currentNode.IsKey || neighbours.Count() > 2)
                {
                    var edge = new Edge(distance, doors);
                    AddEdge(edges, parentNode, currentNode, edge);
                    parentNode = currentNode;
                    doors = new char[0];
                    distance = 0;
                }

                foreach (var nextNode in neighbours)
                {
                    if (visited.Contains(nextNode))
                    {
                        continue;
                    }

                    if (nextNode.IsDoor)
                    {
                        var nextDoors = new List<char>(doors);
                        nextDoors.Add(char.ToLower(nextNode.Type));
                        backlog.Enqueue((nextNode, parentNode, distance + 1, nextDoors));
                    }
                    else
                    {
                        backlog.Enqueue((nextNode, parentNode, distance + 1, doors));
                    }
                }
            }

            AddEntranceEdges(sourceNode, edges);

            keyMapping[sourceNode.Type] = sourceNode.Position;
            return new Graph(sourceNode, edges, keyMapping);
        }

        private void AddEntranceEdges(Node sourceNode, Dictionary<Node, Dictionary<Node, Edge>> edges)
        {
            // This could be nicer... Building the graph assumes a tree and the nodes around the entrance
            // have mini cycles, so not all edges are added properly
            if (sourceNode.Position == (40, 40))
            {
                var n = nodes[(40, 39)];
                var nw = nodes[(39, 39)];
                var w = nodes[(39, 40)];
                var sw = nodes[(39, 41)];
                var s = nodes[(40, 41)];
                var se = nodes[(41, 41)];
                var e = nodes[(41, 40)];
                var ne = nodes[(41, 39)];

                AddEdge(edges, n, ne, new Edge(1, new char[0]));
                AddEdge(edges, ne, e, new Edge(1, new char[0]));
                AddEdge(edges, e, se, new Edge(1, new char[0]));
                AddEdge(edges, se, s, new Edge(1, new char[0]));
                AddEdge(edges, s, sw, new Edge(1, new char[0]));
                AddEdge(edges, sw, w, new Edge(1, new char[0]));
                AddEdge(edges, w, nw, new Edge(1, new char[0]));
                AddEdge(edges, nw, n, new Edge(1, new char[0]));
            }
        }

        private void AddEdge(Dictionary<Node, Dictionary<Node, Edge>> edges, Node startNode, Node endNode, Edge edge)
        {
            AddEdgeOneWay(edges, startNode, endNode, edge);
            AddEdgeOneWay(edges, endNode, startNode, edge);
        }

        private void AddEdgeOneWay(Dictionary<Node, Dictionary<Node, Edge>> edges, Node startNode, Node endNode, Edge edge)
        {
            if (!edges.ContainsKey(startNode))
            {
                edges[startNode] = new Dictionary<Node, Edge>();
            }

            edges[startNode][endNode] = edge;
        }

        private IEnumerable<Node> GetNeighbours((int X, int Y) position)
        {
            var nexts = new List<Node>();
            foreach ((var dx, var dy) in offsets)
            {
                var x = position.X + dx;
                var y = position.Y + dy;
                
                if (nodes.ContainsKey((x, y)))
                {
                    nexts.Add(nodes[(x, y)]);
                }
            }

            return nexts;
        }
    }
}