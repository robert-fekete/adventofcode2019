﻿using System.Collections.Generic;
using System.Diagnostics;

namespace AdventOfCode2019.Day18
{
    [DebuggerDisplay("W:{Weight}")]
    internal class Edge
    {
        public Edge(int weight, IEnumerable<char> doors)
        {
            Weight = weight;
            Doors = doors;
        }

        public int Weight { get; }
        public IEnumerable<char> Doors { get; }
    }
}
