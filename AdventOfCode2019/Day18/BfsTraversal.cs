﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class BfsTraversal<T>
    {
        private readonly T initialValue;
        private readonly Action<Node, T> visitCallback;
        private readonly Func<T, Edge, T> accumulatorConverter;

        public BfsTraversal(T initialValue, Action<Node, T> visitCallback, Func<T, Edge, T> accumulatorConverter)
        {
            this.initialValue = initialValue;
            this.visitCallback = visitCallback;
            this.accumulatorConverter = accumulatorConverter;
        }

        public void Traverse(Node source, Dictionary<Node, Dictionary<Node, Edge>> edgeList)
        {
            var backlog = new Queue<(Node, T)>();
            backlog.Enqueue((source, initialValue));
            var visited = new HashSet<Node>();

            while (backlog.Any())
            {
                (var current, var accumulator) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                visitCallback(current, accumulator);

                var currentEdges = edgeList[current];
                foreach (var next in currentEdges.Keys)
                {
                    var nextAccumulator = accumulatorConverter(accumulator, currentEdges[next]);
                    backlog.Enqueue((next, nextAccumulator));
                }
            }
        }
    }
}
