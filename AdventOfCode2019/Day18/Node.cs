﻿using System.Diagnostics;

namespace AdventOfCode2019.Day18
{
    [DebuggerDisplay("{Position} - {Type}")]
    internal class Node
    {
        public const char EntranceToken = '@';
        public Node((int, int) position, char type)
        {
            Position = position;
            Type = type;
        }

        public (int, int) Position { get; }
        public char Type { get; }
        public bool IsDoor => char.IsUpper(Type);
        public bool IsKey => char.IsLower(Type);
        public bool IsEmpty => !IsDoor && !IsKey;
        public bool IsEntrance => Type == EntranceToken;
    }
}
