﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class Solver : ISolver
    {
        public int DayNumber => 18;

        public string FirstExpected => "4668";

        public string SecondExpected => "1910";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "########################",
                    "#f.D.E.e.C.b.A.@.a.B.c.#",
                    "######################.#",
                    "#d.....................#",
                    "########################",
                }, "86")
                .AddTestCase(new[]
                {
                    "########################",
                    "#...............b.C.D.f#",
                    "#.######################",
                    "#.....@.a.B.c.d.A.e.F.g#",
                    "########################",
                }, "132")
                .AddTestCase(new[]
                {
                    "#################",
                    "#i.G..c...e..H.p#",
                    "########.########",
                    "#j.A..b...f..D.o#",
                    "########@########",
                    "#k.E..a...g..B.n#",
                    "########.########",
                    "#l.F..d...h..C.m#",
                    "#################",
                }, "136")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "#######",
                    "#a.#Cd#",
                    "##...##",
                    "##.@.##",
                    "##...##",
                    "#cB#.b#",
                    "#######",
                }, "8")
                .AddTestCase(new[]
                {
                    "#############",
                    "#DcBa.#.GhKl#",
                    "#.###...#I###",
                    "#e#d#.@.#j#k#",
                    "###C#...###J#",
                    "#fEbA.#.FgHi#",
                    "#############",
                }, "32")
                .AddTestCase(new[]
                {
                    "#############",
                    "#g#f.D#..h#l#",
                    "#F###e#E###.#",
                    "#dCba...BcIJ#",
                    "#####.@.#####",
                    "#nK.L...G...#",
                    "#M###N#H###.#",
                    "#o#m..#i#jk.#",
                    "#############",
                }, "72")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var parser = new MapParser();
            return Execute(parser.ParseFirst, input);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var parser = new MapParser();
            return Execute(parser.ParseSecond, input);
        }

        public string Execute(Func<IEnumerable<string>, MapFactory> parser, IEnumerable<string> input)
        {
            var factory = parser(input);

            var maps = factory.CreateMaps();
            var explorers = maps.Select(m => new Explorer(Node.EntranceToken, 0, m));
            
            var simulator = new Simulator(explorers);
            var shortestDistance = simulator.GetShortestCollection();

            return shortestDistance.ToString();
        }
    }
}
