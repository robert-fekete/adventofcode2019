﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day18
{
    internal class Explorer
    {
        private readonly Dictionary<long, int> cache = new Dictionary<long, int>();
        private readonly Map map;
        private readonly char position;

        public Explorer(char position, int distance, Map map)
        {
            this.position = position;
            this.map = map;
            TotalDistance = distance;
        }
        public Explorer(char position, int distance, Map map, Dictionary<long, int> cache)
            :this(position, distance, map)
        {
            this.cache = cache;
        }

        public int TotalDistance { get; }

        public bool IsFinished(IEnumerable<char> collectedKeys)
        {
            return map.HasAllKeys(collectedKeys);
        }

        public IEnumerable<char> GetNextKeys(IEnumerable<char> collectedKeys)
        {
            return map.GetAccessibleKeys(collectedKeys);
        }

        public Explorer Move(char key)
        {
            var distance = map.GetDistance(position, key);
            return new Explorer(key, TotalDistance + distance, map, cache);
        }

        public Explorer RestoreState(IEnumerable<char> collectedKeys)
        {
            var hash = GetHash(collectedKeys, position);
            var totalDistance = TotalDistance + cache[hash];

            return new Explorer(position, totalDistance, map, cache);
        }

        public bool HasState(IEnumerable<char> collectedKeys)
        {
            var hash = GetHash(collectedKeys, position);
            return cache.ContainsKey(hash);
        }

        public void SaveState(int newDistance, IEnumerable<char> collectedKeys)
        {
            var hash = GetHash(collectedKeys, position);
            cache[hash] = newDistance - TotalDistance;
        }

        private static long GetHash(IEnumerable<char> previousNodes, char currentNode)
        {
            unchecked // Overflow is fine, just wrap
            {
                //return string.Join("", previousNodes.OrderBy(a => a)) + "_" + currentNode;
                int hash = (int)2166136261;
                foreach (var key in previousNodes.OrderBy(a => a))
                {
                    hash = (hash * 16777619) ^ key.GetHashCode();
                }

                hash = (hash * 16777633) ^ currentNode.GetHashCode();
                return hash;
            }
        }
    }
}
