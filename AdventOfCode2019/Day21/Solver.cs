﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day21
{
    internal class Solver : ISolver
    {
        public int DayNumber => 21;

        public string FirstExpected => "19357544";

        public string SecondExpected => "1144498646";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }
        
        public string ExecuteFirst(IEnumerable<string> input)
        {
            var code = new List<string>()
            {
                "OR A T",  // If either A, B or C is a whole and D is ground, then jump
                "AND B T",
                "AND C T",
                "NOT T J",
                "AND D J"
            };

            return Execute(input, "WALK", code).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var code = new List<string>()
            {
                "OR A T",  // If A,B and F is ground, than wait with the jump
                "AND B T",
                "AND F T",
                "OR T J",
                "NOT J J",
                "OR H J",  // If H is also ground, then DON'T wait with the jump (but do double hop if needed)
                "OR A T",  // If either A, B or C is a whole and D is ground, then jump
                "AND B T",
                "AND C T",
                "NOT T T",
                "AND T J",
                "AND D J",

            };

            return Execute(input, "RUN", code).ToString();
        }

        public long Execute(IEnumerable<string> input, string endCommand, IList<string> code)
        {
            var factory = new IntCodeFactory();
            var computer = factory.Create(input.First().Split(','));

            code.Add(endCommand);

            var springScript = string.Join('\n', code) + '\n';
            computer.Execute(new long[0]);

            var lastOutput = computer.Execute(springScript.Select<char, long>(c => c));
            if (lastOutput.Count() > 1)
            {
                foreach (var character in lastOutput)
                {
                    //Console.Write((char)character);
                }
            }

            return lastOutput.Last();
        }
    }
}
