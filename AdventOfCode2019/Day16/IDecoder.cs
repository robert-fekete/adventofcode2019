﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day16
{
    internal interface IDecoder
    {
        IEnumerable<int> Decode(int iterations);
    }
}
