﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day16
{
    internal class SimpleSignal
    {
        public SimpleSignal(IEnumerable<int> signal)
        {
            Data = signal;
        }

        public IEnumerable<int> Data { get; }
    }
}
