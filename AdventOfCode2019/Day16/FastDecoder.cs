﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day16
{
    internal class FastDecoder : IDecoder
    {
        private readonly IEnumerable<int> signal;

        public FastDecoder(IEnumerable<int> signal)
        {
            this.signal = signal;
        }

        public IEnumerable<int> Decode(int iterations)
        {
            IList<int> current = new List<int>(signal);

            for (int it = 0; it < iterations; it++)
            {
                current = GenerateSignal(current);
            }

            return current;
        }

        private IList<int> GenerateSignal(IList<int> signal)
        {
            var next = new List<int>();
            var acc = 0;
            for (int i = signal.Count - 1; i >= 0; i--)
            {
                acc += signal[i];
                next.Add(Math.Abs(acc % 10));
            }
            next.Reverse();

            return next;
        }
    }
}
