﻿namespace AdventOfCode2019.Day16
{
    internal class DecoderFactory
    {
        private readonly int[] pattern;

        public DecoderFactory(int[] pattern)
        {
            this.pattern = pattern;
        }

        public IDecoder Create(SimpleSignal signal)
        {
            return new SlowDecoder(signal.Data, pattern);
        }

        public IDecoder Create(RepeatingSignal signal)
        {
            return new FastDecoder(signal.SignificantData);
        }
    }
}
