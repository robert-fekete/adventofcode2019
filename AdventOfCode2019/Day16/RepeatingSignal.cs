﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day16
{
    internal class RepeatingSignal
    {
        private readonly IEnumerable<int> signal;
        private readonly int repeatNumber;

        public RepeatingSignal(IEnumerable<int> signal, int repeatNumber)
        {
            this.signal = signal;
            this.repeatNumber = repeatNumber;
        }

        public IEnumerable<int> SignificantData
        {
            get
            {
                var offsetLength = int.Parse(string.Join("", this.signal.Take(7)));
                var prefixLength = signal.Count();
                var fullRepeatToSkip = offsetLength / prefixLength;

                var charactersToSkip = offsetLength - fullRepeatToSkip * prefixLength;
                var signalLength = prefixLength * repeatNumber;
                var firstSignalLength = prefixLength - charactersToSkip;

                var remainingLength = signalLength - offsetLength - firstSignalLength;
                var fullRepeatInSignal = remainingLength / prefixLength;

                var signalSuffix = signal.Skip(charactersToSkip)
                                         .Take(firstSignalLength)
                                         .Concat(Enumerable.Repeat(signal, fullRepeatInSignal).SelectMany(i => i));

                return signalSuffix;
            }
        }
    }
}
