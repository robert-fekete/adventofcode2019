﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day16
{
    internal class SlowDecoder : IDecoder
    {
        private readonly IEnumerable<int> signal;
        private int[] pattern;

        public SlowDecoder(IEnumerable<int> signal, int[] pattern)
        {
            this.signal = signal;
            this.pattern = pattern;
        }

        public IEnumerable<int> Decode(int iterations)
        {
            var patterns = GeneratePatterns(signal.Count());

            IList<int> current = new List<int>(signal);
            for (int it = 0; it < iterations; it++)
            {
                current = GenerateSignal(current, patterns);
            }

            return current;
        }

        private IList<int> GenerateSignal(IList<int> signal, IList<IList<int>> patterns)
        {
            var next = new List<int>();
            for (int i = 0; i < signal.Count; i++)
            {
                var acc = 0;
                for (int j = 0; j < signal.Count; j++)
                {
                    acc += (signal[j] * patterns[i][j]);
                }
                next.Add(Math.Abs(acc % 10));
            }

            return next;
        }

        private IList<IList<int>> GeneratePatterns(int count)
        {
            var rows = new List<IList<int>>();
            for (int i = 1; i <= count; i++)
            {
                var row = new List<int>();
                for (int j = 1; j <= count; j++)
                {
                    var period = pattern.Length * i;
                    var index = (j % period) / i;
                    row.Add(pattern[index]);
                }
                rows.Add(row);
            }

            return rows;
        }
    }
}
