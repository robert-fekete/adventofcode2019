﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day16
{
    internal class Solver : ISolver
    {
        public int DayNumber => 16;

        public string FirstExpected => "70856418";

        public string SecondExpected => "87766336";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("80871224585914546619083218645595", 100, "24176176")
                .AddTestCase("12345678", 4, "01029498")
                .AddTest((i, p) => First(i, p))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("03036732577212944063491565474664", (100, 10000), "84462026")
                .AddTest((i, p) => Second(i, p.Item1, p.Item2))
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input.First(), 100);
        }

        private string First(string input, int iterations)
        {
            var factory = new DecoderFactory(new[] { 0, 1, 0, -1 });
            var signal = new SimpleSignal(input.Select(c => c - '0'));

            var decoder = factory.Create(signal);
            var decodedSignal = decoder.Decode(iterations);
            var prefix = decodedSignal.Take(8);

            return string.Join("", prefix);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return Second(input.First(), 100, 10000);
        }

        private string Second(string input, int iterations, int repeatNumber)
        {
            var factory = new DecoderFactory(new[] { 0, 1, 0, -1 });
            var signal = new RepeatingSignal(input.Select(c => c - '0'), repeatNumber);

            var decoder = factory.Create(signal);
            var decodedSignal = decoder.Decode(iterations);
            var prefix = decodedSignal.Take(8);

            return string.Join("", prefix);
        }
    }
}
