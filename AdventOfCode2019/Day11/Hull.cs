﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day11
{
    internal class Hull
    {
        private readonly HashSet<(int, int)> painted = new HashSet<(int, int)>();
        private readonly HashSet<(int, int)> whites = new HashSet<(int, int)>();

        public int PaintSize => painted.Count;

        public void PaintBlack((int, int) position)
        {
            painted.Add(position);
            if (whites.Contains(position))
            {
                whites.Remove(position);
            }
        }

        public void PaintWhite((int, int) position)
        {
            painted.Add(position);
            whites.Add(position);
        }

        public int GetColor((int X, int Y) position)
        {
            return whites.Contains(position) ? 1 : 0;
        }

        public IEnumerable<IEnumerable<bool>> ConvertToBoolArrays()
        {
            var minX = whites.Min(p => p.Item1);
            var maxX = whites.Max(p => p.Item1);
            var minY = whites.Min(p => p.Item2);
            var maxY = whites.Max(p => p.Item2);

            var rows = new List<List<bool>>();
            for (int y = minY; y <= maxY; y++)
            {
                var row = new List<bool>();
                for (int x = minX; x <= maxX; x++)
                {
                    row.Add(whites.Contains((x, y)));
                }
                rows.Add(row);
            }

            return rows;
        }
    }
}
