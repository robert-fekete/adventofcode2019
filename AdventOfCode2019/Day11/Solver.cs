﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Tools.OCR;

namespace AdventOfCode2019.Day11
{
    internal class Solver : ISolver
    {
        public int DayNumber => 11;

        public string FirstExpected => "2226";

        public string SecondExpected => "HBGLZKLF";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var code = input.First().Split(',');
            var hull = PaintHull(code, 0L);

            return hull.PaintSize.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var code = input.First().Split(',');
            var hull = PaintHull(code, 1L);

            var image = hull.ConvertToBoolArrays();
            var ocr = new OcrAnalyzer();

            return ocr.ReadText(image);
        }

        private Hull PaintHull(string[] code, long input)
        {
            var factory = new IntCodeFactory();
            var computer = factory.Create(code);

            var hull = new Hull();
            var robot = new Robot(hull);
            while (!computer.Finished)
            {
                var output = computer.Execute(new[] { input });
                input = robot.ProcessData(output);
            }

            return hull;
        }
    }
}
