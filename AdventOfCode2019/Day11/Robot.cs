﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day11
{
    internal class Robot
    {
        private readonly Dictionary<Direction, (int Dx, int Dy)> offsets = new Dictionary<Direction, (int, int)>()
        {
            { Direction.Left, (-1, 0) },
            { Direction.Up, (0, -1) },
            { Direction.Right, (1, 0) },
            { Direction.Down, (0, 1) },
        };
        private readonly Hull hull;

        private (int X, int Y) currentPosition = (0, 0);
        private Direction currentDirection = Direction.Up;

        public Robot(Hull hull)
        {
            this.hull = hull;
        }

        internal int ProcessData(IEnumerable<long> input)
        {
            var color = input.First();
            Paint(color);

            var turn = input.Skip(1).First();
            Move(turn);

            return hull.GetColor(currentPosition);
        }

        private void Paint(long color)
        {
            if (color == 0)
            {
                hull.PaintBlack(currentPosition);
            }
            else if (color == 1)
            {
                hull.PaintWhite(currentPosition);
            }
            else
            {
                throw new InvalidOperationException($"Invalid color: {color}");
            }
        }

        private void Move(long turn)
        {
            if (turn == 0)
            {
                currentDirection = TurnLeft();
            }
            else if (turn == 1)
            {
                currentDirection = TurnRight();
            }
            else
            {
                throw new InvalidOperationException($"Invalid direction: {turn}");
            }
            var offset = offsets[currentDirection];
            currentPosition.X += offset.Dx;
            currentPosition.Y += offset.Dy;
        }

        private Direction TurnLeft()
        {
            switch (currentDirection)
            {
                case Direction.Left:
                    return Direction.Down;
                case Direction.Up:
                    return Direction.Left;
                case Direction.Right:
                    return Direction.Up;
                case Direction.Down:
                    return Direction.Right;
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private Direction TurnRight()
        {
            switch (currentDirection)
            {
                case Direction.Left:
                    return Direction.Up;
                case Direction.Up:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Left;
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private enum Direction
        {
            Left,
            Up,
            Right,
            Down
        }
    }
}
