﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day4
{
    internal class Password
    {
        private readonly int value;
        private readonly int repeatingMaxLimit;
        private readonly IEnumerable<int> digits;

        private bool hasDoubleDigits = false;
        private bool onlyDecreasing = true;


        public Password(int value, int repeatingMaxLimit)
        {
            this.value = value;
            this.repeatingMaxLimit = repeatingMaxLimit;
            digits = GetDigits();
        }

        public bool IsValid()
        {
            var repeating = 0;

            var previous = digits.First();
            foreach (var current in digits.Skip(1))
            {
                if (previous == current)
                {
                    repeating++;
                }
                else
                {
                    UpdateDoubleDigits(repeating);
                    repeating = 0;
                }
                if (previous > current)
                {
                    onlyDecreasing &= false;
                }
                previous = current;
            }
            UpdateDoubleDigits(repeating);

            return onlyDecreasing && hasDoubleDigits;
        }

        private void UpdateDoubleDigits(int repeating)
        {
            if (repeating >= 1 && repeating < repeatingMaxLimit)
            {
                hasDoubleDigits |= true;
            }
        }

        private IEnumerable<int> GetDigits()
        {
            var digits = new List<int>();
            var temp = value;
            while (temp > 0)
            {
                var lastDigit = temp % 10;
                digits.Add(lastDigit);
                temp /= 10;
            }

            digits.Reverse();
            return digits;
        }
    }
}
