﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day4
{
    internal class Solver : ISolver
    {
        public int DayNumber => 4;

        public string FirstExpected => "2090";

        public string SecondExpected => "1419";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(111111, true)
                .AddTestCase(223450, false)
                .AddTestCase(123789, false)
                .AddTestCase(122345, true)
                .AddTest(i =>
                {
                    var calculator = new Calculator(10);
                    var count = calculator.GetNumberOfPasswords(i, i);

                    return count == 1;
                })
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(112233, true)
                .AddTestCase(123444, false)
                .AddTestCase(111122, true)
                .AddTest(i =>
                {
                    var calculator = new Calculator(2);
                    var count = calculator.GetNumberOfPasswords(i, i);

                    return count == 1;
                })
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var parts = input.First().Split('-');
            var from = int.Parse(parts[0]);
            var to = int.Parse(parts[1]);

            var calculator = new Calculator(10);
            var count = calculator.GetNumberOfPasswords(from, to);

            return count.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var parts = input.First().Split('-');
            var from = int.Parse(parts[0]);
            var to = int.Parse(parts[1]);

            var calculator = new Calculator(2);
            var count = calculator.GetNumberOfPasswords(from, to);

            return count.ToString();
        }
    }
}
