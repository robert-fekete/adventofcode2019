﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day4
{
    internal class Calculator
    {
        private readonly int repeatingMaxLimit;

        public Calculator(int repeatingMaxLimit)
        {
            this.repeatingMaxLimit = repeatingMaxLimit;
        }

        public int GetNumberOfPasswords(int from, int to)
        {
            var count = 0;
            for (int i = from; i <= to; i++)
            {
                var password = new Password(i, repeatingMaxLimit);
                if (password.IsValid())
                {
                    count++;
                }
            }

            return count;
        }
    }
}
