﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day25
{
    internal class Solver : ISolver
    {
        public int DayNumber => 25;

        public string FirstExpected => "67635328";

        public string SecondExpected => "";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(false);
        }

        public string ExecuteFirst(IEnumerable<string> intCode)
        {
            var factory = new IntCodeFactory();
            var computer = factory.Create(intCode.First().Split(','));

            //PlayInteractive(computer);

            var asciiComputer = new AsciiCapableComputer(computer);
            var explorer = new Explorer(asciiComputer);
            var graph = explorer.Explore();

            var items = new[]
            {
                "dark matter",
                "tambourine",
                "astrolabe",
                "monolith"
            };
            var mse6 = new MouseDroid(graph, asciiComputer);
            mse6.CollectItems(items);
            mse6.GoToRoom("Security Checkpoint");
            var password = mse6.TalkToSanta(Direction.North.Serialize()); // North is an assumption based on the interactive play, might be different for other inputs

            return password;
        }

        private static void PlayInteractive(IIntCodeComputer computer)
        {
            string nextInstruction = string.Empty;
            while (!computer.Finished)
            {
                var input = nextInstruction.Select(c => (long)c);
                var output = computer.Execute(input);
                foreach (var character in output)
                {
                    Console.Write((char)character);
                }
                nextInstruction = Console.ReadLine() + '\n';
            }
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            throw new NotImplementedException();
        }
    }
}
