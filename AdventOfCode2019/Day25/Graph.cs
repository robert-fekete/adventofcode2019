﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day25
{
    public class Graph
    {
        private readonly Dictionary<string, IEnumerable<(string, string)>> edges;
        private readonly Dictionary<string, string> items;

        public Graph(string root, Dictionary<string, IEnumerable<(string, string)>> edges, Dictionary<string, string> items)
        {
            Root = root;
            this.edges = edges;
            this.items = items;
        }

        public string Root { get; }

        internal string GetRoom(string item)
        {
            return items[item];
        }

        internal IEnumerable<(string, string)> GetNeighbours(string node)
        {
            return edges[node];
        }
    }
}