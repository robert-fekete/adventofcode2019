﻿using AdventOfCode2019.IntCode;
using System.Linq;

namespace AdventOfCode2019.Day25
{
    internal class AsciiCapableComputer
    {
        private readonly IIntCodeComputer computer;

        public AsciiCapableComputer(IIntCodeComputer computer)
        {
            this.computer = computer;
        }

        public string Execute(string input)
        {
            var values = input.Select(c => (long)c);
            var output = computer.Execute(values);

            return string.Join("", output.Select(c => (char)c));
        }
    }
}
