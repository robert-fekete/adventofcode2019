﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day25
{
    internal class GraphBuilder
    {
        private string root = string.Empty;
        private readonly List<string> nodes = new List<string>();
        private readonly Dictionary<string, string> items = new Dictionary<string, string>();
        private readonly Dictionary<string, List<(string, string)>> edges = new Dictionary<string, List<(string, string)>>();

        public void Clear()
        {
            root = string.Empty;
            nodes.Clear();
            items.Clear();
            edges.Clear();
        }

        public void AddRoot(string node)
        {
            root = node;
            AddNode(node);
        }

        public void AddEdge(string startNode, string endNode, string direction)
        {
            if (!nodes.Contains(startNode) || !nodes.Contains(endNode))
            {
                throw new InvalidOperationException("Node list doesn't contain either the start or the end node");
            }

            edges[startNode].Add((endNode, direction));
        }

        public void AddItem(string node, string item)
        {
            items[item] = node;
        }

        public void AddNode(string node)
        {
            nodes.Add(node);
            edges[node] = new List<(string, string)>();
        }

        public Graph Create()
        {
            if (string.IsNullOrEmpty(root))
            {
                throw new InvalidOperationException("Root is not set");
            }
            if (!nodes.Any())
            {
                throw new InvalidOperationException("There isn't any node set");
            }
            if (!edges.Any())
            {
                throw new InvalidOperationException("There isn't any edge set");
            }

            return new Graph(root, edges.ToDictionary(kvp => kvp.Key, kvp => kvp.Value as IEnumerable<(string, string)>), items);
        }
    }
}
