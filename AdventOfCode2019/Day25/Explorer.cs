﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day25
{
    internal class Explorer
    {
        private readonly GraphBuilder builder = new GraphBuilder();
        private readonly AsciiCapableComputer computer;

        public Explorer(AsciiCapableComputer computer)
        {
            this.computer = computer;
        }

        public Graph Explore()
        {
            builder.Clear();

            ExploreRec(new Direction(), "Space");

            return builder.Create();
        }

        private void ExploreRec(Direction direction, string previousRoom)
        {
            var input = direction.Serialize();
            
            var output = computer.Execute(input);

            var currentRoom = GetRoom(output);
            if (direction.IsRoot)
            {
                builder.AddRoot(currentRoom);
            }
            else
            {
                builder.AddNode(currentRoom);
                builder.AddEdge(previousRoom, currentRoom, direction.Serialize());
                builder.AddEdge(currentRoom, previousRoom, direction.TraceBack.Serialize());
            }

            var items = GetItems(output);
            if (items.Any())
            {
                foreach(var item in items)
                {
                    builder.AddItem(currentRoom, item);
                }
            }

            if (currentRoom != "Security Checkpoint") // Avoid the "Pressure-sensitive" floor until we collect the items
            {
                foreach (var nextDirection in Direction.Values)
                {
                    if (!nextDirection.IsTracingBack(direction) && output.Contains(nextDirection.Serialize()))
                    {
                        ExploreRec(nextDirection, currentRoom);
                    }
                }
            }

            if (!direction.IsRoot)
            {
                var traceBack = direction.TraceBack;
                var traceBackInput = traceBack.Serialize();
                computer.Execute(traceBackInput);
            }
        }

        private IEnumerable<string> GetItems(string text)
        {
            var lines = text.Split('\n');
            var index = 0;
            while (index < lines.Length && !lines[index].StartsWith("Items here"))
            {
                index++;
            }
            index++;

            if (index >= lines.Length)
            {
                return Enumerable.Empty<string>();
            }

            var items = new List<string>();
            while (!string.IsNullOrEmpty(lines[index]))
            {
                var item = lines[index].Substring(2);
                items.Add(item);
                index++;
            }

            return items;
        }

        private string GetRoom(string text)
        {
            var lines = text.Split('\n');
            foreach (var line in lines)
            {
                if (line.StartsWith("=="))
                {
                    var parts = line.Split('=');
                    return parts[2].Trim();
                }
            }

            throw new InvalidOperationException("There is no room name for this room");
        }
    }
}
