﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day25
{
    internal class MouseDroid
    {
        private readonly Graph graph;
        private readonly AsciiCapableComputer computer;
        private string currentRoom;

        public MouseDroid(Graph graph, AsciiCapableComputer computer)
        {
            this.graph = graph;
            currentRoom = graph.Root;
            this.computer = computer;
        }

        public void CollectItems(IEnumerable<string> items)
        {
            foreach (var item in items) 
            {
                var room = graph.GetRoom(item);
                GoToRoom(room);
                Take(item);
            }
        }

        public void GoToRoom(string room)
        {
            var path = FindPath(currentRoom, room);
            foreach(var direction in path)
            {
                Go(direction);
            }
            currentRoom = room;
        }

        public void Go(string direction)
        {
            var output = computer.Execute(direction);
        }

        public string TalkToSanta(string direction)
        {
            var inventory = computer.Execute("inv\n");
            var output = computer.Execute(direction);

            return ParsePassword(output);
        }

        private IEnumerable<string> FindPath(string source, string target)
        {
            var parent = new Dictionary<string, (string, string)>();
            var backlog = new Queue<(string, (string, string))>();
            var visited = new HashSet<string>();

            backlog.Enqueue((source, ("Nono", "Oh, nono")));
            while (backlog.Any()) 
            {
                (var current, var previous) = backlog.Dequeue();
                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                parent[current] = previous;

                if(current == target)
                {
                    break;
                }

                foreach((var next, var direction) in graph.GetNeighbours(current))
                {
                    backlog.Enqueue((next, (current, direction)));
                }
            }

            var path = new List<string>();
            var node = target;
            while(node != source)
            {
                (var previous, var direction) = parent[node];
                path.Add(direction);
                node = previous;
            }

            path.Reverse();
            return path;
        }

        private void Take(string item)
        {
            var output = computer.Execute($"take {item}\n");
        }

        private string ParsePassword(string output)
        {
            var lines = output.Split('\n');
            var passwordLine = lines.Where(l => !string.IsNullOrEmpty(l)).Last();

            var words = passwordLine.Split(' ');
            var password = words.First(w => int.TryParse(w, out var _));

            return password;
        }
    }
}
