﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day25
{
    public class Direction
    {
        private static readonly Dictionary<Directions, Directions> traceBack = new Dictionary<Directions, Directions>()
        {
            { Directions.North, Directions.South },
            { Directions.East, Directions.West },
            { Directions.South, Directions.North },
            { Directions.West, Directions.East },
        };

        public static readonly Direction North = new Direction(Directions.North);
        public static readonly Direction East = new Direction(Directions.East);
        public static readonly Direction South = new Direction(Directions.South);
        public static readonly Direction West = new Direction(Directions.West);
        public static IEnumerable<Direction> Values => new[]
        {
            North,
            East,
            South,
            West
        };

        private readonly Directions direction;

        public Direction()
        {
            direction = Directions.None;
        }

        private Direction(Directions direction)
        {
            this.direction = direction;
        }

        public bool IsRoot => direction == Directions.None;

        public Direction TraceBack => new Direction(traceBack[direction]);

        public bool IsTracingBack(Direction previous)
        {
            return previous.direction != Directions.None && direction == traceBack[previous.direction];
        }

        public string Serialize()
        {
            return direction.ToString().ToLower() + '\n';
        }

        private enum Directions{
            North,
            East,
            South,
            West,
            None
        }
    }
}
