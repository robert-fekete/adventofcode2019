﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day19
{
    internal class LinearRegression
    {
        private IEnumerable<(int, int)> dataPoints;
        private double a;
        private double b;

        public LinearRegression(IEnumerable<(int, int)> dataPoints)
        {
            this.dataPoints = dataPoints;
            var coefficients = CalculateCoefficients();
            a = coefficients.Item1;
            b = coefficients.Item2;
        }
        
        public int CalculateValue(int x)
        {
            return (int)(b * x + a);
        }

        private (double, double) CalculateCoefficients()
        {
            var sumX = 0L;
            var sumY = 0L;
            var sumXY = 0L;
            var sumXX = 0L;
            var sumYY = 0L;
            var n = 0;
            foreach((var x, var y) in dataPoints)
            {
                sumX += x;
                sumXX += x * x;
                sumY += y;
                sumYY += y * y;
                sumXY += x * y;
                n++;
            }

            a = ((double)(sumY * sumXX - sumX * sumXY)) / (n * sumXX - sumX * sumX);
            b = ((double)(n * sumXY - sumX * sumY)) / (n * sumXX - sumX * sumX);

            return (a, b);
        }
    }
}