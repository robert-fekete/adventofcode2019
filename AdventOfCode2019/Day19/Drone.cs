﻿using AdventOfCode2019.IntCode;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day19
{
    internal class Drone : IDrone
    {
        private readonly IEnumerable<string> code;
        private readonly IntCodeFactory factory = new IntCodeFactory();

        public Drone(IEnumerable<string> code)
        {
            this.code = code;
        }

        public int Operations { get; private set; } = 0;

        public bool Deploy((long X, long Y) coordinates)
        {
            Operations++;
            var computer = factory.Create(code);
            var output = computer.Execute(new long[] { coordinates.X, coordinates.Y });

            return output.First() == 1;
        }
    }
}
