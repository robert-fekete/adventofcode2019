﻿namespace AdventOfCode2019.Day19
{
    internal interface IDrone
    {
        bool Deploy((long X, long Y) coordinates);
        int Operations { get; }
    }
}