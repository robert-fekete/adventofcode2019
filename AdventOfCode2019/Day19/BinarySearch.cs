﻿using System;

namespace AdventOfCode2019.Day19
{
    class BinarySearch
    {

        public int Search(int low, int high, Predicate<int> predicate)
        {
            while (low < high)
            {
                var mid = low + (high - low) / 2;
                if (predicate(mid))
                {
                    high = mid;
                }
                else
                {
                    low = mid + 1;
                }
            }

            if (predicate(low))
            {
                return low;
            }
            else
            {
                return high;
            }
        }
    }
}
