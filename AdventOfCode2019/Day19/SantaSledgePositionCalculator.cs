﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day19
{
    internal class SantaSledgePositionCalculator
    {
        private readonly Mapper mapper;
        private readonly BinarySearch search = new BinarySearch();

        public SantaSledgePositionCalculator(Mapper mapper)
        {
            this.mapper = mapper;
        }

        public (int, int) GetPosition(int size, int tolarence)
        {
            var regression = CreateRegression();
            var indexPrediction = regression.CalculateValue(size);

            var startIndex = Math.Max(0, indexPrediction - tolarence);
            var endIndex = indexPrediction + tolarence;

            var index = search.Search(startIndex, endIndex, i => IsFit(i, size));
            
            var bottomIndex = index + size - 1;
            (var start, var _) = mapper.GetBeamSegment(bottomIndex);

            return (start, index);
        }

        private bool IsFit(int index, int size)
        {
            (var _, var topEnd) = mapper.GetBeamSegment(index);
            var bottomIndex = index + size - 1;
            (var bottomStart, var _) = mapper.GetBeamSegment(bottomIndex);

            return (size <= topEnd - bottomStart + 1);
        }

        private LinearRegression CreateRegression()
        {
            var dataPoints = new List<(int, int)>();
            var index = 0;
            for (int i = 2; i < 12; i++)
            {
                while (!IsFit(index, i))
                {
                    index++;
                }
                dataPoints.Add((i, index));
                continue;
            }

            return new LinearRegression(dataPoints);
        }
    }
}
