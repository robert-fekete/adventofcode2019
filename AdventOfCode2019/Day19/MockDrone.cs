﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day19
{
    internal class MockDrone : IDrone
    {
        private readonly string[] map;

        public MockDrone(IEnumerable<string> map)
        {
            this.map = map.Select(l => l.Replace("O", "#")).ToArray();
        }

        public int Operations { get; private set; }

        public bool Deploy((long X, long Y) coordinates)
        {
            Operations++;
            if (map.Length <= coordinates.Y)
            {
                return false;
            }
            var line = map[coordinates.Y];
            var x = (int)coordinates.X;
            if (line.Length <= x)
            {
                return false;
            }

            return line[x] != '.';
        }
    }
}
