﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day19
{
    internal class Mapper
    {
        private const int Tolarence = 4;

        private Dictionary<int, (int, int)> cache = new Dictionary<int, (int, int)>();
        private LinearRegression startCoordinates;
        private LinearRegression endCoordinates;

        private readonly IDrone drone;

        public Mapper(IDrone drone)
        {
            this.drone = drone;
            Initialize(10);
            startCoordinates = new LinearRegression(cache.Select(kvp => (kvp.Key, kvp.Value.Item1)));
            endCoordinates = new LinearRegression(cache.Select(kvp => (kvp.Key, kvp.Value.Item2)));
        }

        public (int, int) GetBeamSegment(int lineIndex)
        {
            if (cache.ContainsKey(lineIndex))
            {
                return cache[lineIndex];
            }

            var startSegment = GetStartSegment(lineIndex);
            var endSegment = GetEndSegment(lineIndex);
            var segment = (startSegment, endSegment);

            cache[lineIndex] = segment;
            return segment;
        }

        private void Initialize(int lines)
        {
            cache.Clear();
            for (int y = 0; y < lines; y++)
            {
                cache[y] = GetBeamSegmentHard(y);
            }
        }

        private int GetStartSegment(int lineIndex)
        {
            var index = startCoordinates.CalculateValue(lineIndex) - Tolarence;
            
            if(drone.Deploy((index, lineIndex)))
            {
                while (drone.Deploy((index, lineIndex)))
                {
                    index--;
                }
                return index + 1;
            }
            else
            {
                while (!drone.Deploy((index, lineIndex)))
                {
                    index++;
                }
                return index;
            }
        }

        private int GetEndSegment(int lineIndex)
        {
            var index = endCoordinates.CalculateValue(lineIndex) + Tolarence;
            
            if (drone.Deploy((index, lineIndex)))
            {
                while (drone.Deploy((index, lineIndex)))
                {
                    index++;
                }
                return index - 1;
            }
            else
            {
                while (!drone.Deploy((index, lineIndex)))
                {
                    index--;
                }
                return index;
            }
        }

        private (int, int) GetBeamSegmentHard(int lineIndex)
        {
            var start = 0;
            var end = 0;
            var inBeam = false;
            var index = 0;
            while (!inBeam)
            {
                var value = drone.Deploy((index, lineIndex));
                if (value)
                {
                    start = index;
                    inBeam = true;
                    break;
                }
                index++;
            }
            while (inBeam)
            {
                var value = drone.Deploy((index, lineIndex));
                if (!value)
                {
                    end = index;
                    break;
                }
                index++;
            }

            return (start, end - 1);
        }
    }
}
