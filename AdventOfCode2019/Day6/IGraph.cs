﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day6
{
    internal interface IGraph
    {
        string Root { get; }

        IEnumerable<string> GetNeighbours(string current);
    }
}