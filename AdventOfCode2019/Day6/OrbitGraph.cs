﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day6
{
    internal class OrbitGraph : IGraph
    {
        private string root;
        private Dictionary<string, IEnumerable<string>> neighbours;

        public OrbitGraph(string root, Dictionary<string, IEnumerable<string>> neighbours)
        {
            this.root = root;
            this.neighbours = neighbours;
        }

        public string Root => "COM";

        public IEnumerable<string> GetNeighbours(string current)
        {
            if (neighbours.ContainsKey(current))
            {
                return neighbours[current];
            }
            else
            {
                return Enumerable.Empty<string>();
            }
        }

        public static OrbitGraph FromInput(IEnumerable<string> input)
        {
            var neighbours = new Dictionary<string, List<string>>();
            foreach (var line in input)
            {
                var parts = line.Split(')');
                if (!neighbours.ContainsKey(parts[0]))
                {
                    neighbours[parts[0]] = new List<string>();
                }
                neighbours[parts[0]].Add(parts[1]);
            }

            return new OrbitGraph("COM", ToDictionary(neighbours));
        }

        private static Dictionary<string, IEnumerable<string>> ToDictionary(Dictionary<string, List<string>> neighbours)
        {
            return neighbours.ToDictionary<KeyValuePair<string, List<string>>, string, IEnumerable<string>>(kvp => kvp.Key, kvp => kvp.Value);
        }
    }
}
