﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdventOfCode2019.Day6
{
    internal class PathBuilder
    {
        private readonly Dictionary<string, string> parents = new Dictionary<string, string>();
        private readonly string root;

        public PathBuilder(string root)
        {
            this.root = root;
        }

        internal void AddParent(string node, string parent)
        {
            parents[node] = parent;
        }

        internal IEnumerable<string> GetPath(string node)
        {
            var path = new List<string>()
            {
                node
            };
            var current = node;
            while (current != root)
            {
                current = parents[current];
                path.Add(current);
            }

            path.Reverse();
            return path;
        }
    }
}
