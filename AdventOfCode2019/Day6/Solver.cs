﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day6
{
    internal class Solver : ISolver
    {
        public int DayNumber => 6;

        public string FirstExpected => "117672";

        public string SecondExpected => "277"; 

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "COM)B",
                    "B)C",
                    "C)D",
                    "D)E",
                    "E)F",
                    "B)G",
                    "G)H",
                    "D)I",
                    "E)J",
                    "J)K",
                    "K)L"
                }, 42)
                .AddTest(i => First(i))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "COM)B",
                    "B)C",
                    "C)D",
                    "D)E",
                    "E)F",
                    "B)G",
                    "G)H",
                    "D)I",
                    "E)J",
                    "J)K",
                    "K)L",
                    "K)YOU",
                    "I)SAN"
                }, 4)
                .AddTest(i => Second(i))
                .Build();

        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input).ToString();
        }

        private int First(IEnumerable<string> input)
        {
            var graph = OrbitGraph.FromInput(input);
            var dfs = new DfsTraversal(graph);

            var total = 0;
            dfs.TraverseAll(i => total += i);

            return total;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return Second(input).ToString();
        }

        private int Second(IEnumerable<string> input)
        {
            var graph = OrbitGraph.FromInput(input);
            var dfs = new DfsTraversal(graph);

            var path1 = dfs.FindShortestPath("YOU").ToArray();
            var path2 = dfs.FindShortestPath("SAN").ToArray();

            var iter = 0;
            while(iter + 1 < path1.Length && iter + 1 < path2.Length && path1[iter + 1] == path2[iter + 1])
            {
                iter++;
            }

            var youPathLength = path1.Length - iter - 1; // -1 for YOU
            var sanPathLength = path2.Length - iter - 1; // -1 for SAN
            var totalPlanets = youPathLength + sanPathLength - 1; // -1 for common planet
            var totalJumps = totalPlanets - 1; // -1,because there are one less edges then nodes on a path

            return totalJumps;
        }
    }
}
