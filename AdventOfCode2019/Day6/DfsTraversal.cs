﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day6
{
    internal class DfsTraversal
    {
        private readonly IGraph graph;

        public DfsTraversal(IGraph graph)
        {
            this.graph = graph;
        }

        public IEnumerable<string> FindShortestPath(string target)
        {
            var stack = new Stack<(string parent, string node)>();
            var visited = new HashSet<string>();
            var builder = new PathBuilder(graph.Root);

            stack.Push(("", graph.Root));
            while (stack.Count > 0)
            {
                (var parent, var current) = stack.Pop();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);
                builder.AddParent(current, parent);

                if (current == target)
                {
                    return builder.GetPath(current);
                }

                foreach (var next in graph.GetNeighbours(current))
                {
                    stack.Push((current, next));
                }
            }

            throw new InvalidOperationException($"Target node is not found in the graph: {target}");
        }

        public void TraverseAll(Action<int> callback)
        {
            var stack = new Stack<(int depth, string node)>();
            var visited = new HashSet<string>();

            stack.Push((0, graph.Root));
            while(stack.Count > 0)
            {
                (var depth, var current) = stack.Pop();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                callback(depth);

                foreach(var next in graph.GetNeighbours(current))
                {
                    stack.Push((depth + 1, next));
                }
            }
        }
    }
}
