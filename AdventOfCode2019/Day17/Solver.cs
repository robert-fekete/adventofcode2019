﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day17
{
    internal class Solver : ISolver
    {
        public int DayNumber => 17;

        public string FirstExpected => "13580";

        public string SecondExpected => "1063081";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "..#..........",
                    "..#..........",
                    "#######...###",
                    "#.#...#...#.#",
                    "#############",
                    "..#...#...#..",
                    "..#####...^..",
                }, 76)
                .AddTest(s =>
                {
                    var builder = new MapBuilder();
                    var input = string.Join("\n", s);
                    input += "\n\n"; // Input has double line break at the end ¯\_(ツ)_/¯
                    builder.Add(input.Select<char, long>(c => c));

                    var map = builder.Build();
                    return map.GetAlignmentParameter();
                })
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "#######...#####",
                    "#.....#...#...#",
                    "#.....#...#...#",
                    "......#...#...#",
                    "......#...###.#",
                    "......#.....#.#",
                    "^########...#.#",
                    "......#.#...#.#",
                    "......#########",
                    "........#...#..",
                    "....#########..",
                    "....#...#......",
                    "....#...#......",
                    "....#...#......",
                    "....#####......",
                }, "A,B,C,B,A,C\nR,8,R,8\nR,4,R,4\nR,8,L,6,L,2\nn\n")
                .AddTest(s =>
                {
                    var input = string.Join("\n", s);
                    input += "\n\n"; // Input has double line break at the end ¯\_(ツ)_/¯

                    var result = GetRoutines(input.Select<char, long>(c => c));
                    return result;
                })
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var factory = new IntCodeFactory();
            var computer = factory.Create(input.First().Split(','));

            var builder = new MapBuilder();
            while (!computer.Finished)
            {
                var output = computer.Execute(new long[0]);
                builder.Add(output);
            }

            var map = builder.Build();
            return map.GetAlignmentParameter().ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var code = input.First().Split(',');
            var factory = new IntCodeFactory();
            var computer = factory.Create(code);

            
            var output = computer.Execute(new long[0]);
            var routines = GetRoutines(output);
            code[0] = "2";
            computer = factory.Create(code);
            
            var dust = computer.Execute(routines.Select<char, long>(c => c));
            return dust.Last().ToString();
        }

        private static string GetRoutines(IEnumerable<long> input)
        {
            var builder = new MapBuilder();
            builder.Add(input);
            var map = builder.Build();
            var roomba = Roomba.FromInput(input, map);
            var instructions = roomba.GetFullTraversal();

            var grouping = new PatternGrouping(instructions);
            (var mainRoutine, var subRoutines) = grouping.GetGrouping(3, 10, 20);

            return GenerateInput(mainRoutine, subRoutines);
        }

        private static string GenerateInput(IEnumerable<char> mainRoutine, Dictionary<char, IEnumerable<string>> subRoutines)
        {
            var inputBuilder = new StringBuilder();
            inputBuilder.Append(string.Join(",", mainRoutine));
            inputBuilder.Append("\n");
            foreach (var routineName in subRoutines.Keys.OrderBy(c => c))
            {
                inputBuilder.Append(string.Join(",", subRoutines[routineName]));
                inputBuilder.Append("\n");
            }
            inputBuilder.Append("n");
            inputBuilder.Append("\n");

            return inputBuilder.ToString();
        }
    }
}