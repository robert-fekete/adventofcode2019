﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day17
{
    internal class Roomba
    {
        private readonly Map map;
        private int x;
        private int y;
        private Direction direction;

        private Roomba(Map map, int x, int y, Direction direction)
        {
            this.map = map;
            this.x = x;
            this.y = y;
            this.direction = direction;
        }

        public IEnumerable<string> GetFullTraversal()
        {
            var instructions = new List<string>();
            while (!IsFinished())
            {
                if (CanMoveForward())
                {
                    var steps = 0;
                    while (CanMoveForward())
                    {
                        MoveForward();
                        steps++;
                    }
                    instructions.Add(steps.ToString());
                }
                else if (CanTurnLeft())
                {
                    TurnLeft();
                    instructions.Add("L");
                }
                else if (CanTurnRight())
                {
                    TurnRight();
                    instructions.Add("R");
                }
            }

            return instructions;
        }

        private bool IsFinished()
        {
            return !CanMoveForward() && !CanTurnLeft() && !CanTurnRight();
        }

        private bool CanTurnRight()
        {
            switch (direction)
            {
                case Direction.Up:
                    return map.HasScafolding((x + 1, y));
                case Direction.Right:
                    return map.HasScafolding((x, y + 1));
                case Direction.Down:
                    return map.HasScafolding((x - 1, y));
                case Direction.Left:
                    return map.HasScafolding((x, y - 1));
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private bool CanTurnLeft()
        {
            switch (direction)
            {
                case Direction.Up:
                    return map.HasScafolding((x - 1, y));
                case Direction.Right:
                    return map.HasScafolding((x, y - 1));
                case Direction.Down:
                    return map.HasScafolding((x + 1, y));
                case Direction.Left:
                    return map.HasScafolding((x, y + 1));
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private bool CanMoveForward()
        {
            var nextPosition = GetNextMove();
            return map.HasScafolding(nextPosition);
        }

        private (int X, int Y) GetNextMove()
        {
            switch (direction)
            {
                case Direction.Up:
                    return (x, y - 1);
                case Direction.Right:
                    return (x + 1, y);
                case Direction.Down:
                    return (x, y + 1);
                case Direction.Left:
                    return (x - 1, y);
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private void TurnRight()
        {
            switch (direction)
            {
                case Direction.Up:
                    direction = Direction.Right;
                    break;
                case Direction.Right:
                    direction = Direction.Down;
                    break;
                case Direction.Down:
                    direction = Direction.Left;
                    break;
                case Direction.Left:
                    direction = Direction.Up;
                    break;
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private void TurnLeft()
        {
            switch (direction)
            {
                case Direction.Up:
                    direction = Direction.Left;
                    break;
                case Direction.Right:
                    direction = Direction.Up;
                    break;
                case Direction.Down:
                    direction = Direction.Right;
                    break;
                case Direction.Left:
                    direction = Direction.Down;
                    break;
                default:
                    throw new InvalidOperationException("Invalid direction");
            }
        }

        private void MoveForward()
        {
            var nextPosition = GetNextMove();
            x = nextPosition.X;
            y = nextPosition.Y;
        }

        public static Roomba FromInput(IEnumerable<long> output, Map map)
        {
            int x = 0;
            int y = 0;
            foreach (char c in output)
            {
                if (c == '\n' && x != 0)
                {
                    x = 0;
                    y++;
                    continue;
                }
                if (c == '^')
                {
                    return new Roomba(map, x, y, Direction.Up);
                }
                if (c == '>')
                {
                    return new Roomba(map, x, y, Direction.Right);
                }
                if (c == 'v')
                {
                    return new Roomba(map, x, y, Direction.Down);
                }
                if (c == '<')
                {
                    return new Roomba(map, x, y, Direction.Left);
                }
                x++;
            }

            throw new InvalidOperationException("No robot was found");
        }

        private enum Direction
        {
            Left,
            Up,
            Right,
            Down
        }
    }
}
