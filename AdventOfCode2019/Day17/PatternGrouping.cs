﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day17
{
    internal class PatternGrouping
    {
        private readonly string[] input;

        private char nextGroupName;
        private bool[] isTaken;
        
        /// <summary>
        /// Input might have items with multiple characters, when moving forward needs more than 9 steps
        /// </summary>
        /// <param name="input"></param>
        public PatternGrouping(IEnumerable<string> input)
        {
            this.input = input.ToArray();
            isTaken = new bool[0];
        }

        public (IEnumerable<char>, Dictionary<char, IEnumerable<string>>) GetGrouping(int maxNumberOfGroups, int maxGroupsUsed, int maxGroupSize)
        {
            var groups = FindGroups(maxGroupSize);
            while (groups.Item1.Count() > maxGroupsUsed || groups.Item2.Keys.Count > maxNumberOfGroups || groups.Item2.Values.Any(g => g.Count() > maxGroupSize))
            {
                maxGroupSize--;
                groups = FindGroups(maxGroupSize);
            }

            return groups;
        }

        private (IEnumerable<char>, Dictionary<char, IEnumerable<string>>) FindGroups(int maxGroupSize)
        {
            nextGroupName = 'A';
            isTaken = new bool[input.Length];

            var mainSequence = new Dictionary<int, char>();
            var groups = new Dictionary<char, IEnumerable<string>>();

            var index = 0;
            while (index < input.Length)
            {
                var length = 0;
                var numberOfReoccurrence = 0;
                var totalLength = 0;
                while (index + length < input.Length && !isTaken[index + length] && totalLength <= maxGroupSize)
                {
                    length += 2;
                    totalLength = CalculateRealGroupLength(index, length);

                    numberOfReoccurrence = FindReoccurrences(index, length);
                    if (numberOfReoccurrence < 1)
                    {
                        length -= 2;
                        break;
                    }
                }

                if (length == 0)
                {
                    index++;
                    continue;
                }

                var name = nextGroupName++;
                groups[name] = input.Skip(index).Take(length).ToArray();
                RegisterOccurrences(name, groups[name], mainSequence);

                index += length;
            }

            return (mainSequence.OrderBy(kvp => kvp.Key).Select(kvp => kvp.Value), groups);
        }

        private int FindReoccurrences(int index, int length)
        {
            var pattern = input.Skip(index).Take(length).ToArray();
            var reoccurrences = 0;
            for(int i = index + 1; i + length <= input.Length; i++)
            {
                bool isMatch = IsPatternMatch(i, pattern);
                if (isMatch)
                {
                    reoccurrences++;
                }
            }

            return reoccurrences;
        }

        private void RegisterOccurrences(char name, IEnumerable<string> group, Dictionary<int, char> mainSequence)
        {
            var pattern = group.ToArray();
            for(int i = 0; i + pattern.Length <= input.Length; i++)
            {
                if (isTaken[i])
                {
                    continue;
                }

                var isMatch = IsPatternMatch(i, pattern);
                if (isMatch)
                {
                    mainSequence[i] = name;
                    for(int offset = 0; offset < pattern.Length; offset++)
                    {
                        isTaken[i + offset] = true;
                    }
                }
            }
        }

        private bool IsPatternMatch(int index, string[] pattern)
        {
            var isMatch = true;
            for (int offset = 0; offset < pattern.Length; offset++)
            {
                if (input[index + offset] != pattern[offset] || isTaken[index + offset])
                {
                    isMatch = false;
                    break;
                }
            }

            return isMatch;
        }

        private int CalculateRealGroupLength(int index, int length)
        {
            return input.Skip(index).Take(length).Sum(s => s.Length)    // String lengths
                        + length - 1;                                   // For commas
        }
    }
}
