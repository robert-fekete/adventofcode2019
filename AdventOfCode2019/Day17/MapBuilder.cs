﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day17
{
    internal class MapBuilder
    {
        private static char[] ScafoldingCharacters = new[] { '#', '^', 'v', '>', '<' };
        private readonly HashSet<(int, int)> cells = new HashSet<(int, int)>();

        private int width = 0;
        private int x = 0;
        private int y = 0;

        public void Add(IEnumerable<long> output)
        {
            foreach(char c in output)
            {
                if (c == '\n' && x != 0)
                {
                    if (width == 0)
                    {
                        width = x;
                    }
                    x = 0;
                    y++;
                    continue;
                }

                if (ScafoldingCharacters.Contains(c))
                {
                    cells.Add((x, y));
                }

                x++;
            }
        }

        public Map Build()
        {
            return new Map(width, y, cells);
        }
    }
}