﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day17
{
    internal class Map
    {
        private readonly int width;
        private readonly int height;
        private readonly HashSet<(int, int)> cells;

        public Map(int width, int height, IEnumerable<(int, int)> cells)
        {
            this.width = width;
            this.height = height;
            this.cells = new HashSet<(int, int)>(cells);
        }

        public bool HasScafolding((int, int y) position)
        {
            return cells.Contains(position);
        }

        public int GetAlignmentParameter()
        {
            var score = 0;
            for(int y = 0; y < height; y++)
            {
                for(int x = 0; x < width; x++)
                {
                    if (IsIntersection(x, y))
                    {
                        score += (y) * (x);
                    }
                }
            }

            return score;
        }

        public bool IsIntersection(int x, int y)
        {
            return cells.Contains((x, y)) &&
                (x == 0 || cells.Contains((x - 1, y))) &&
                (x == width - 1 || cells.Contains((x + 1, y))) &&
                (y == 0 || cells.Contains((x, y - 1))) &&
                (x == height - 1 || cells.Contains((x, y + 1)));
        }
    }
}