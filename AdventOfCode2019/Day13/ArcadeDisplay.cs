﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AdventOfCode2019.Day13
{
    internal class ArcadeDisplay
    {
        private readonly Dictionary<(int X, int Y), TileType> display = new Dictionary<(int, int), TileType>();

        public int BlockNumber => display.Count(kvp => kvp.Value == TileType.Block);
        public int BallPosition => display.Where(kvp => kvp.Value == TileType.Ball).First().Key.X;
        public int PaddlePosition => display.Where(kvp => kvp.Value == TileType.Paddle).First().Key.X;
        public int Score { get; private set; }

        public void Process(IEnumerable<long> output)
        {
            var values = output.ToArray();
            for (int i = 0; i < values.Length; i += 3)
            {
                var x = Convert.ToInt32(values[i]);
                var y = Convert.ToInt32(values[i + 1]);

                if (x == -1 && y == 0)
                {
                    Score = Convert.ToInt32(values[i + 2]);
                }
                else
                {
                    var type = (TileType)values[i + 2];
                    display[(x, y)] = type;
                }
            }
        }

        public void Print()
        {
            var minX = display.Min(kvp => kvp.Key.X);
            var maxX = display.Max(kvp => kvp.Key.X);
            var minY = display.Min(kvp => kvp.Key.Y);
            var maxY = display.Max(kvp => kvp.Key.Y);

            Thread.Sleep(50);
            Console.Clear();
            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (display.ContainsKey((x, y)))
                    {
                        Console.Write(GetDisplayCharacter(display[(x, y)]));
                    }
                    else
                    {
                        Console.Write('?');
                    }
                }
                Console.WriteLine();
            }
        }

        private char GetDisplayCharacter(TileType tileType)
        {
            switch (tileType)
            {
                case TileType.Empty:
                    return ' ';
                case TileType.Wall:
                    return 'X';
                case TileType.Block:
                    return '#';
                case TileType.Paddle:
                    return '_';
                case TileType.Ball:
                    return '@';
                default:
                    throw new InvalidOperationException("Invalid tile type");
            }
        }

        private enum TileType
        {
            Empty = 0,
            Wall = 1,
            Block = 2,
            Paddle = 3,
            Ball = 4
        }
    }
}