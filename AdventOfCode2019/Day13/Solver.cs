﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day13
{
    internal class Solver : ISolver
    {
        public int DayNumber => 13;

        public string FirstExpected => "355";

        public string SecondExpected => "18371";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var factory = new IntCodeFactory();
            var code = input.First().Split(',');
            var computer = factory.Create(code);
            var display = new ArcadeDisplay();

            while (!computer.Finished)
            {
                var output = computer.Execute(new long[0]);
                display.Process(output);
            }

            //display.Print();
            return display.BlockNumber.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> puzzleInput)
        {
            var code = puzzleInput.First().Split(',').ToArray();
            code[0] = "2";

            var factory = new IntCodeFactory();
            var computer = factory.Create(code);
            var display = new ArcadeDisplay();
            var player = new ArcadePlayer(display);

            var input = new long[0];
            while (!computer.Finished)
            {
                var output = computer.Execute(input);
                display.Process(output);
                input = new long[] { player.GetMextMove() };

                //display.Print();
            }

            return display.Score.ToString();
        }
    }
}
