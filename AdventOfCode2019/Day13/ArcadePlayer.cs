﻿using System;

namespace AdventOfCode2019.Day13
{
    internal class ArcadePlayer
    {
        private readonly ArcadeDisplay display;

        public ArcadePlayer(ArcadeDisplay display)
        {
            this.display = display;
        }

        internal int GetMextMove()
        {
            var ballPosition = display.BallPosition;
            var paddlePosition = display.PaddlePosition;

            if (ballPosition == paddlePosition)
            {
                return 0;
            }
            else if (ballPosition < paddlePosition)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }
    }
}
