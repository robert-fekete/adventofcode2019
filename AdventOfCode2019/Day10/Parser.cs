﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day10
{
    internal class Parser
    {
        public static AsteroidCollection FromInput(IEnumerable<string> input)
        {
            var asteroids = new List<Position>();
            var y = 0;
            foreach(var line in input)
            {
                for(int x = 0; x < line.Length; x++)
                {
                    if (line[x] == '#')
                    {
                        asteroids.Add(new Position(x, y));
                    }
                }
                y++;
            }

            return new AsteroidCollection(asteroids);
        }
    }
}
