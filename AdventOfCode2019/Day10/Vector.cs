﻿using System;

namespace AdventOfCode2019.Day10
{
    public class Vector
    {
        private int x;
        private int y;

        public Vector(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        private double Angle => Math.Atan2(y, x);

        public decimal GetAngleWithOffset(double offset)
        {
            var angle = Convert.ToDecimal(Angle + offset);
            return angle >= 0 ? angle : angle + Convert.ToDecimal(Math.PI * 2);
        }

        public override bool Equals(object obj)
        {
            return obj is Vector vector &&
                   x == vector.x &&
                   y == vector.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }
    }
}