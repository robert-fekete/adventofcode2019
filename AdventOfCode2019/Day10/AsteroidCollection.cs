﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day10
{
    internal class AsteroidCollection
    {
        private readonly Position[] asteroids;

        public AsteroidCollection(IEnumerable<Position> asteroids)
        {
            this.asteroids = asteroids.ToArray();
        }

        public (Position Position, int Rank) GetOrigin()
        {
            var max = -1;
            Position? origin = null;
            for (int i = 0; i < asteroids.Length; i++)
            {
                var grouping = GetAsteroidGrouping(asteroids[i]);
                var vectors = grouping.GetGroupVectors();
                var asteoridsInSight = vectors.Count();
                if (asteoridsInSight > max)
                {
                    origin = asteroids[i];
                    max = asteoridsInSight;
                }
            }

            if (origin == null)
            {
                throw new InvalidOperationException("No origin can be found");
            }

            return (origin, max);
        }

        public Position GetVaporizedAsteoridInOrder(int order)
        {
            (var origin, var _) = GetOrigin();
            var grouping = GetAsteroidGrouping(origin);

            var polarGrouping = grouping.GetPolarGrouping();
            var angles = polarGrouping.Keys.OrderBy(d => d);

            var vaporized = 0;
            while (true)
            {
                foreach(var angle in angles)
                {
                    if (polarGrouping[angle].Count == 0)
                    {
                        continue;
                    }

                    var target = polarGrouping[angle][0];
                    polarGrouping[angle].RemoveAt(0);
                    vaporized++;

                    if (vaporized == order)
                    {
                        return target;
                    }
                }
            }
        }

        private AsteroidGrouping GetAsteroidGrouping(Position origin)
        {
            var grouping = new AsteroidGrouping(origin);
            for (int i = 0; i < asteroids.Length; i++)
            {
                var other = asteroids[i];
                if (other.Equals(origin))
                {
                    continue;
                }
                grouping.AddToGroup(other);
            }

            return grouping;
        }
    }
}
