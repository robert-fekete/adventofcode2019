﻿using System;

namespace AdventOfCode2019.Day10
{
    internal class Position
    {
        private readonly int x;
        private readonly int y;

        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector GetVectorToPosition(Position other)
        {
            var dx = other.x - x;
            var dy = other.y - y;
            var length = GetHighestCommonDenominator(dx, dy);
            return new Vector(dx / length, dy / length);
        }

        private int GetHighestCommonDenominator(int aRaw, int bRaw)
        {
            var a = Math.Abs(aRaw);
            var b = Math.Abs(bRaw);
            if (a == 0)
            {
                return b;
            }
            if (b == 0)
            {
                return a;
            }
            var min = Math.Min(a, b);
            for(int i = min; i > 0; i--)
            {
                if (a % i == 0 && b % i == 0)
                {
                    return i;
                }
            }

            return 1;
        }

        public double GetDistanceFromPosition(Position other)
        {
            var dx = other.x - x;
            var dy = other.y - y;

            return Math.Sqrt(dx * dx + dy * dy);
        }

        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   x == position.x &&
                   y == position.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }

        public override string ToString()
        {
            return $"({x}:{y})";
        }
        public string ToString(int offset)
        {
            return $"{x * offset + y}";
        }
    }
}
