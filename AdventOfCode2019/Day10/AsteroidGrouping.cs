﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day10
{
    internal class AsteroidGrouping
    {
        private readonly Dictionary<Vector, List<Position>> groups = new Dictionary<Vector, List<Position>>();
        private readonly Position origin;

        public AsteroidGrouping(Position origin)
        {
            this.origin = origin;
        }

        public void AddToGroup(Position asteroid)
        {
            var vector = origin.GetVectorToPosition(asteroid);
            if (!groups.ContainsKey(vector))
            {
                groups[vector] = new List<Position>();
            }
            groups[vector].Add(asteroid);
        }

        public Dictionary<decimal, List<Position>> GetPolarGrouping()
        {
            return groups.ToDictionary<KeyValuePair<Vector, List<Position>>, decimal, List<Position>>(kvp => kvp.Key.GetAngleWithOffset(Math.PI / 2),
                                                                                                             kvp => kvp.Value.OrderBy(p => origin.GetDistanceFromPosition(p)).ToList());
        }

        public IEnumerable<Vector> GetGroupVectors()
        {
            return groups.Keys;
        }
    }
}
