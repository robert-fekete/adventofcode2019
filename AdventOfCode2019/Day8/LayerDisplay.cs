﻿using System;

namespace AdventOfCode2019.Day8
{
    internal class LayerDisplay
    {
        public void DrawConsole(Layer layer)
        {
            for(int y = 0; y < layer.Height; y++)
            {
                for(int x = 0; x < layer.Width; x++)
                {
                    var pixel = layer.GetPixel(x, y);
                    var color = pixel == 1 ? "#" : " ";
                    Console.Write(color);
                }
                Console.WriteLine();
            }
        }
    }
}
