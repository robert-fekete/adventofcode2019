﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day8
{
    internal class LayerRenderer
    {
        private readonly Layer[] layers;
        private const int TRANSPARENT_PIXEL = 2;

        public LayerRenderer(IEnumerable<Layer> layers)
        {
            this.layers = layers.ToArray();
        }

        public Layer Render()
        {
            var width = layers[0].Width;
            var height = layers[0].Height;
            var rows = new List<List<int>>();
            for(int y = 0; y < height; y++)
            {
                var pixels = new List<int>();
                for(int x = 0; x < width; x++)
                {
                    var layerIndex = 0;
                    while(layers[layerIndex].GetPixel(x, y) == TRANSPARENT_PIXEL)
                    {
                        layerIndex++;
                    }
                    var pixel = layers[layerIndex].GetPixel(x, y);
                    pixels.Add(pixel);
                }
                rows.Add(pixels);
            }

            return new Layer(rows);
        }
    }
}
