﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Tools.OCR;

namespace AdventOfCode2019.Day8
{
    internal class Solver : ISolver
    {
        public int DayNumber => 8;

        public string FirstExpected => "1716";

        public string SecondExpected => "KFABY";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("122312012111", 6)
                .AddTest(s => First(s, 3, 2))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("0222112222120000", (2, 2), "0110")
                .AddTest((i, p) =>
                {
                    var layers = new LayerParser().Parse(i, p.Item1, p.Item2);
                    var rendered = new LayerRenderer(layers).Render();

                    return rendered.ToString();
                })
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input.First(), 25, 6).ToString();
        }

        private int First(string input, int width, int height)
        {
            var layers = new LayerParser().Parse(input, width, height);

            var minZeros = layers.Min(l => l.Count(0));
            var selectedLayer = layers.Where(l => l.Count(0) == minZeros).Single();

            return selectedLayer.Count(1) * selectedLayer.Count(2);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var layers = new LayerParser().Parse(input.First(), 25, 6);
            var rendered = new LayerRenderer(layers).Render();

            var image = rendered.ConvertToBoolArrays();
            var ocr = new OcrAnalyzer();

            return ocr.ReadText(image);
        }
    }
}
