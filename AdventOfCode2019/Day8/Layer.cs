﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day8
{
    internal class Layer
    {
        private List<List<int>> rows;

        public Layer(List<List<int>> rows)
        {
            this.rows = rows;
        }

        public int Width => rows[0].Count;
        public int Height => rows.Count;

        public int Count(int digit)
        {
            return rows.SelectMany(l => l).Count(d => d == digit);
        }

        public int GetPixel(int x, int y)
        {
            return rows[y][x];
        }

        public IEnumerable<IEnumerable<bool>> ConvertToBoolArrays()
        {
            return rows.Select(r => r.Select(c => c == 1));
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            foreach(var pixel in rows.SelectMany(l => l))
            {
                builder.Append(pixel);
            }

            return builder.ToString();
        }
    }
}
