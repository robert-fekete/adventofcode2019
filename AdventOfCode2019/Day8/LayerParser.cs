﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day8
{
    internal class LayerParser
    {
        public IEnumerable<Layer> Parse(string input, int width, int height)
        {
            var layers = new List<Layer>();

            for (int iter = 0; iter < input.Length; iter+= width * height)
            {
                var rows = new List<List<int>>();
                for (int dy = 0; dy < height; dy++)
                {
                    var pixels = new List<int>();
                    for (int dx = 0; dx < width; dx++)
                    {
                        var index = iter + dy * width + dx;
                        var digit = input[index] - '0';
                        pixels.Add(digit);
                    }
                    rows.Add(pixels);
                }
                layers.Add(new Layer(rows));
            }

            return layers;
        }
    }
}