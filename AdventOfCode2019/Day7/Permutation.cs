﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day7
{
    internal class Permutation
    {
        private readonly int[] numbers;

        public Permutation(int[] numbers)
        {
            this.numbers = numbers;
        }

        public IEnumerable<IEnumerable<int>> GetAll()
        {
            var stack = new Stack<List<int>>();
            stack.Push(new List<int>());

            while(stack.Count > 0)
            {
                var permutation = stack.Pop();
                if (permutation.Count == numbers.Length)
                {
                    yield return permutation;
                }

                foreach (var number in numbers)
                {
                    if (!permutation.Contains(number))
                    {
                        var next = permutation.ToList();
                        next.Add(number);
                        stack.Push(next);
                    }
                }
            }
        }
    }
}
