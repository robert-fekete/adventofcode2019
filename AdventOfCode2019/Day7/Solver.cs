﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using AdventOfCode2019.IntCode.Program;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day7
{
    internal class Solver : ISolver
    {
        public int DayNumber => 7;

        public string FirstExpected => "65464";

        public string SecondExpected => "1518124";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", 43210L)
                .AddTestCase("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", 54321)
                .AddTestCase("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", 65210)
                .AddTest(s => First(s))
                .Build();

        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", 139629729L)
                .AddTestCase("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", 18216)
                .AddTest(s => Second(s))
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input.First()).ToString();
        }

        private long First(string input)
        {
            var code = input.Split(',');
            var permutations = new Permutation(new[] { 0, 1, 2, 3, 4 });
            var factory = new IntCodeFactory();

            var max = 0L;
            foreach (var order in permutations.GetAll())
            {
                var carryOver = 0L;
                foreach (var phaseSetting in order)
                {
                    var computer = factory.Create(code);
                    var output = computer.Execute(new[] { phaseSetting, carryOver });
                    carryOver = output.First();
                }

                if (carryOver > max)
                {
                    max = carryOver;
                }
            }

            return max;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return Second(input.First()).ToString();
        }

        private long Second(string input)
        {
            var code = input.Split(',');
            var permutations = new Permutation(new[] { 5, 6, 7, 8, 9 });
            var factory = new IntCodeFactory();

            var max = 0L;
            foreach (var order in permutations.GetAll())
            {
                var result = CalculateThrusterSignal(code, factory, order.ToArray());
                if (result > max)
                {
                    max = result;
                }
            }

            return max;
        }

        private long CalculateThrusterSignal(string[] code, IntCodeFactory factory, int[] phaseSettings)
        {
            var computers = new List<IIntCodeComputer>();
            var inputs = new List<List<long>>();
            var outputs = new List<List<long>>();
            for (int i = 0; i < 5; i++)
            {
                computers.Add(factory.Create(code));
                inputs.Add(new List<long>() { phaseSettings[i] });
                outputs.Add(new List<long>());
            }
            inputs[0].Add(0);

            while (!computers[4].Finished)
            {
                for (int i = 0; i < 5; i++)
                {
                    var temp = inputs[i].ToArray();
                    inputs[i].Clear();

                    var output = computers[i].Execute(temp);
                    outputs[i].AddRange(output);
                    inputs[ConvertToInputIndex(i)].AddRange(output);
                }
            }

            var result = outputs[4].Last();
            return result;
        }

        private int ConvertToInputIndex(int outputIndex)
        {
            if (outputIndex == 4)
            {
                return 0;
            }
            return outputIndex + 1;
        }
    }
}
