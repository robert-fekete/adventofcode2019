﻿namespace AdventOfCode2019.Day15
{
    internal class Explorer
    {
        private const int RIGHT = 4;
        private const int LEFT = 3;
        private const int UP = 1;
        private const int DOWN = 2;

        public int GetDirection(int previousDirection, bool isLastMoveOk)
        {
            if (isLastMoveOk)
            {
                switch (previousDirection)
                {
                    case RIGHT:
                        return DOWN;
                    case DOWN:
                        return LEFT;
                    case LEFT:
                        return UP;
                    case UP:
                        return RIGHT;
                    default:
                        return RIGHT;
                }
            }
            else
            {
                switch (previousDirection)
                {
                    case RIGHT:
                        return UP;
                    case DOWN:
                        return RIGHT;
                    case LEFT:
                        return DOWN;
                    case UP:
                        return LEFT;
                    default:
                        return LEFT;
                }
            }
        }
    }
}
