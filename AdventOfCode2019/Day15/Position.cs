﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day15
{
    internal class Position
    {
        private readonly Dictionary<int, (int Dx, int Dy)> offsets = new Dictionary<int, (int Dx, int Dy)>()
        {
            { 1, (0, -1) },
            { 2, (0, 1) },
            { 3, (-1, 0) },
            { 4, (1, 0) }
        };

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }

        public Position Move(int direction)
        {
            (var dx, var dy) = offsets[direction];
            return new Position(X + dx, Y + dy);
        }


        public override bool Equals(object obj)
        {
            return obj is Position position &&
                   X == position.X &&
                   Y == position.Y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(X, Y);
        }

        public override string ToString()
        {
            return $"({X}:{Y})";
        }
    }
}
