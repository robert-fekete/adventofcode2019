﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;

namespace AdventOfCode2019.Day15
{
    internal class Solver : ISolver
    {
        public int DayNumber => 15;

        public string FirstExpected => "238";

        public string SecondExpected => "392";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            Map map = DiscoverMap(input);

            if (map.Target == null)
            {
                throw new InvalidOperationException("Couldn't find oxygen tank");
            }

            var traversal = new BfsTraversal(map);
            var length = traversal.GetShortestPath(map.Target);

            return length.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            Map map = DiscoverMap(input);

            if (map.Target == null)
            {
                throw new InvalidOperationException("Couldn't find oxygen tank");
            }

            var traversal = new BfsTraversal(map);
            var length = traversal.GetLongestPath(map.Target);

            return length.ToString();
        }

        private static Map DiscoverMap(IEnumerable<string> input)
        {
            var factory = new IntCodeFactory();
            var code = input.First().Split(',');
            var computer = factory.Create(code);

            var map = new Map();
            var position = new Position(0, 0);
            var explorer = new Explorer();

            var isLastMoveOk = true;
            var direction = 2;
            var moves = 0;
            while (moves < 20 || !position.Equals(new Position(0, 0)))
            {
                moves++;
                direction = explorer.GetDirection(direction, isLastMoveOk);

                var output = computer.Execute(new long[] { direction }).First();
                isLastMoveOk = output != 0;

                var nextPosition = position.Move(direction);
                map.Add(nextPosition, Convert.ToInt32(output));
                if (isLastMoveOk)
                {
                    position = nextPosition;
                }

                //map.Print(position);
                //Thread.Sleep(75);
            }

            return map;
        }
    }
}
