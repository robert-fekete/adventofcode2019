﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day15
{
    internal class BfsTraversal
    {
        private readonly IGraph graph;

        public BfsTraversal(IGraph graph)
        {
            this.graph = graph;
        }

        public int GetShortestPath(Position target)
        {
            var depth = 0;
            Traverse(graph.Root, (p, d) =>
            {
                if (target.Equals(p))
                {
                    depth = d;
                    return true;
                }
                return false;
            });

            return depth;
        }

        public int GetLongestPath(Position source)
        {
            var maxPath = 0;
            Traverse(source, (_, d) =>
            {
                if (maxPath < d)
                {
                    maxPath = d;
                }
                return false;
            });
            
            return maxPath;
        }

        private void Traverse(Position source, Func<Position, int, bool> callback)
        {
            var backlog = new Queue<(Position, int)>();
            backlog.Enqueue((source, 0));
            var visited = new HashSet<Position>();

            while (backlog.Any())
            {
                (var current, var depth) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (callback(current, depth))
                {
                    return;
                }

                foreach (var next in graph.GetNeighbours(current))
                {
                    backlog.Enqueue((next, depth + 1));
                }
            }
        }
    }
}
