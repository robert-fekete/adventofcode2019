﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day15
{
    internal interface IGraph
    {
        Position Root { get; }

        IEnumerable<Position> GetNeighbours(Position current);
    }
}