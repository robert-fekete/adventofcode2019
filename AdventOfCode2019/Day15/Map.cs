﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day15
{
    internal class Map : IGraph
    {
        private readonly HashSet<Position> walls = new HashSet<Position>();

        public Position? Target { get; private set; }

        Position IGraph.Root => new Position(0, 0);

        public void Add(Position position, int type)
        {
            if (type == 2)
            {
                Target = position;
            }
            if (type == 0)
            {
                walls.Add(position);
            }
        }

        IEnumerable<Position> IGraph.GetNeighbours(Position current)
        {
            var offsets = new (int Dx, int Dy)[] { (0, 1), (1, 0), (0, -1), (-1, 0) };
            return offsets.Select(o => new Position(current.X + o.Dx, current.Y + o.Dy))
                .Where(p => !walls.Contains(p));
        }

        public void Print(Position robot)
        {
            if (!walls.Any())
            {
                return;
            }
            var minX = Math.Min(robot.X, walls.Min(w => w.X));
            var maxX = Math.Max(robot.X, walls.Max(w => w.X));
            var minY = Math.Min(robot.Y, walls.Min(w => w.Y));
            var maxY = Math.Max(robot.Y, walls.Max(w => w.Y));

            Console.Clear();
            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    if (walls.Contains(new Position(x, y)))
                    {
                        Console.Write("#");
                    }
                    else if (robot.X == x && robot.Y == y)
                    {
                        Console.Write("@");
                    }
                    else if (Target?.X == x && Target?.Y == y)
                    {
                        Console.Write("X");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
