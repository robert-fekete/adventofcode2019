﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace AdventOfCode2019.Day22
{
    internal class SuffleCompressor
    {
        private readonly BigInteger deckSize;

        public SuffleCompressor(BigInteger deckSize)
        {
            this.deckSize = deckSize;
        }

        internal (BigInteger, BigInteger) Compress(IEnumerable<(ShuffleType, long)> operations)
        {
            BigInteger offset = 0;      // The value of the first element in the deck
            BigInteger increment = 1;   // The value difference between two cards next to each other (same for every two cards in the deck)

            foreach((var operation, BigInteger operand) in operations)
            {
                if (operation == ShuffleType.NewStack)
                {
                    increment *= -1;
                    offset += increment;
                }
                else if (operation == ShuffleType.Cut)
                {
                    offset += operand * increment;
                }
                else if (operation == ShuffleType.Increment)
                {
                    increment *= GetMultiplicativeInverse(operand);
                }
                else
                {
                    throw new InvalidOperationException("Invalid operation to compress");
                }
            }

            return (offset, increment);
        }

        private BigInteger GetMultiplicativeInverse(BigInteger number)
        {
            return BigInteger.ModPow(number, deckSize - 2, deckSize); // inv(a) = a^n-2 % n (Because a * inv(a) = 1 % n)
        }
    }
}
