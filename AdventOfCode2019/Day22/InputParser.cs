﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day22
{
    internal class InputParser
    {
        internal IEnumerable<(ShuffleType, long)> Parse(IEnumerable<string> input)
        {
            var operations = new List<(ShuffleType, long)>();
            foreach (var line in input)
            {
                if (line.StartsWith("deal into new stack"))
                {
                    operations.Add((ShuffleType.NewStack, 0));
                }
                else if (line.StartsWith("deal with increment"))
                {
                    var offset = long.Parse(line.Split(' ').Last());
                    operations.Add((ShuffleType.Increment, offset));
                }
                else if (line.StartsWith("cut"))
                {
                    var offset = long.Parse(line.Split(' ').Last());
                    operations.Add((ShuffleType.Cut, offset));
                }
                else
                {
                    throw new InvalidOperationException("Invalid operation");
                }
            }

            return operations;
        }
    }
}
