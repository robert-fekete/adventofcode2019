﻿namespace AdventOfCode2019.Day22
{
    internal enum ShuffleType
    {
        Cut,
        Increment,
        NewStack
    }
}
