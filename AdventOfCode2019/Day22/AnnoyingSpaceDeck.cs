﻿using System;
using System.Linq;

namespace AdventOfCode2019.Day22
{
    internal class AnnoyingSpaceDeck : IEquatable<AnnoyingSpaceDeck>
    {
        private int[] cards;
        public AnnoyingSpaceDeck(int length)
        {
            cards = Enumerable.Range(0, length).ToArray();
        }

        public AnnoyingSpaceDeck(AnnoyingSpaceDeck other)
        {
            cards = other.cards.ToArray();
        }

        public int FindCard(int value)
        {
            for(int i = 0; i < cards.Length; i++)
            {
                if (cards[i] == value)
                {
                    return i;
                }
            }
            throw new InvalidOperationException($"Can't find the specified cards {value}");
        }

        public void NewStack()
        {
            cards = cards.Reverse().ToArray();
        }

        public void Cut(int offset)
        {
            if (offset < 0)
            {
                Cut(cards.Length + offset);
            }
            else
            {
                var first = cards.TakeLast(cards.Length - offset);
                var second = cards.Take(offset);
                
                cards = first.Concat(second).ToArray();
            }
        }

        public void DealWithIncrement(int offset)
        {
            var newCards = new int[cards.Length];
            var index = 0;
            foreach(var card in cards)
            {
                newCards[index] = card;
                
                index += offset;
                index %= cards.Length;
            }

            cards = newCards;
        }

        public bool Equals(AnnoyingSpaceDeck other)
        {
            return cards.SequenceEqual(other.cards);
        }

        public int GetCard(int position)
        {
            return cards[position];
        }
    }
}
