﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace AdventOfCode2019.Day22
{
    internal class Solver : ISolver
    {
        public int DayNumber => 22;

        public string FirstExpected => "2558";

        public string SecondExpected => "63967243502561";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "deal with increment 7",
                    "deal into new stack",
                    "deal into new stack",
                }, new[] { 0, 3, 6, 9, 2, 5, 8, 1, 4, 7 }, true)
                .AddTestCase(new[]
                {
                    "cut 6",
                    "deal with increment 7",
                    "deal into new stack",
                }, new[] { 3, 0, 7, 4, 1, 8, 5, 2, 9, 6 }, true)
                .AddTestCase(new[]
                {
                    "deal with increment 7",
                    "deal with increment 9",
                    "cut -2",
                }, new[] { 6, 3, 0, 7, 4, 1, 8, 5, 2, 9 }, true)
                .AddTestCase(new[]
                {
                    "deal into new stack",
                    "cut -2",
                    "deal with increment 7",
                    "cut 8",
                    "cut -4",
                    "deal with increment 7",
                    "cut 3",
                    "deal with increment 9",
                    "deal with increment 3",
                    "cut -1",
                }, new[] { 9, 2, 5, 8, 1, 4, 7, 0, 3, 6 }, true)
                .AddTest((input, param) =>
                {
                    var size = 10;
                    var deck = SolveFirst(input, size);
                    var result = new int[size];
                    for (int val = 0; val < size; val++)
                    {
                        var index = deck.FindCard(val);
                        result[index] = val;
                    }
                    return result.SequenceEqual(param);
                })
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase<IEnumerable<string>, (BigInteger, BigInteger, BigInteger), BigInteger>(new[]
                {
                    "deal with increment 7",
                    "cut 26",
                    "deal into new stack",
                    "cut 2",
                    "deal with increment 7",
                    "cut -4",
                    "deal with increment 6",
                    "cut 5",
                    "deal with increment 5",
                    "cut 17",
                    "deal with increment 6",
                    "cut -24",
                    "deal into new stack",
                    "cut 7",
                    "deal into new stack",
                    "cut 28",
                    "deal with increment 15",
                    "cut -19",
                    "deal with increment 7",
                    "deal into new stack",
                    "cut 1",
                    "deal with increment 3",
                }, (10007, 8353, 2523), 3503)
                .AddTestCase(new[]
                {
                    "deal with increment 7",
                    "cut 26",
                    "deal into new stack",
                    "cut 2",
                    "deal with increment 7",
                    "cut -4",
                    "deal with increment 6",
                    "cut 5",
                    "deal with increment 5",
                    "cut 17",
                    "deal with increment 6",
                    "cut -24",
                    "deal into new stack",
                    "cut 7",
                    "deal into new stack",
                    "cut 28",
                    "deal with increment 15",
                    "cut -19",
                    "deal with increment 7",
                    "deal into new stack",
                    "cut 1",
                    "deal with increment 3",
                }, (10007, 6423, 2020), 2151)
                .AddTest((input, p) =>
                {
                    return SolveSecond(input, p.Item1, p.Item2, p.Item3);
                })
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var deck = SolveFirst(input, 10007);
            return deck.FindCard(2019).ToString();
        }

        public AnnoyingSpaceDeck SolveFirst(IEnumerable<string> input, int deckSize)
        {
            var parser = new InputParser();
            var operations = parser.Parse(input);
            var deck = new AnnoyingSpaceDeck(deckSize);
            return Shuffle(operations, deck);
        }

        private static AnnoyingSpaceDeck Shuffle(IEnumerable<(ShuffleType, long)> operations, AnnoyingSpaceDeck deck)
        {
            foreach ((var operation, var operand) in operations)
            {
                switch (operation)
                {
                    case ShuffleType.Cut:
                        deck.Cut((int)operand);
                        break;
                    case ShuffleType.Increment:
                        deck.DealWithIncrement((int)operand);
                        break;
                    case ShuffleType.NewStack:
                        deck.NewStack();
                        break;
                }
            }

            return deck;
        }

        // Based on this approach: https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnkaju/
        public string ExecuteSecond(IEnumerable<string> input)
        {
            BigInteger size = 119315717514047;
            BigInteger iterations = 101741582076661;
            BigInteger position = 2020;

            return SolveSecond(input, size, iterations, position).ToString();
        }

        private BigInteger SolveSecond(IEnumerable<string> input, BigInteger size, BigInteger iterations, BigInteger position)
        {
            var parser = new InputParser();
            var compressor = new SuffleCompressor(size);
            var fullOperations = parser.Parse(input);
            (var offset, var increment) = compressor.Compress(fullOperations);

            var finalOffset = offset * (1 - BigInteger.ModPow(increment, iterations, size)) * GetMultiplicativeInverse(GetModulo(1 - increment, size), size);
            var finalIncrement = BigInteger.ModPow(increment, iterations, size);

            var result = GetModulo(finalIncrement * position + finalOffset, size);
            return result;
        }

        private BigInteger GetMultiplicativeInverse(BigInteger number, BigInteger n)
        {
            return BigInteger.ModPow(number, n - 2, n);
        }

        private static BigInteger GetModulo(BigInteger x, BigInteger m)
        {
            return (x % m + m) % m;
        }

        //public int SolveSecondHard(IEnumerable<string> input, int size, int iteration, int position)
        //{
        //    var parser = new InputParser();
        //    var operations = parser.Parse(input);
        //    var deck = new AnnoyingSpaceDeck(size);

        //    for (int i = 0; i < iteration; i++)
        //    {
        //        Shuffle(operations, deck);
        //    }

        //    return deck.GetCard(position);
        //}
    }
}
