﻿using System;
using System.Collections.Generic;
using AdventOfCode.Framework;

namespace AdventOfCode2019.Day14
{
    internal class Solver : ISolver
    {
        public int DayNumber => 14;

        public string FirstExpected => "220019";

        public string SecondExpected => "5650230";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "9 ORE => 2 A",
                    "8 ORE => 3 B",
                    "7 ORE => 5 C",
                    "3 A, 4 B => 1 AB",
                    "5 B, 7 C => 1 BC",
                    "4 C, 1 A => 1 CA",
                    "2 AB, 3 BC, 4 CA => 1 FUEL"
                }, "165")
                .AddTestCase(new[]
                {
                    "171 ORE => 8 CNZTR",
                    "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
                    "114 ORE => 4 BHXH",
                    "14 VRPVC => 6 BMBT",
                    "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
                    "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
                    "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
                    "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
                    "5 BMBT => 4 WPTQ",
                    "189 ORE => 9 KTJDG",
                    "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
                    "12 VRPVC, 27 CNZTR => 2 XDBXC",
                    "15 KTJDG, 12 BHXH => 5 XCVML",
                    "3 BHXH, 2 VRPVC => 7 MZWV",
                    "121 ORE => 7 VRPVC",
                    "7 XCVML => 6 RJRHP",
                    "5 BHXH, 4 VRPVC => 5 LTCX"
                }, "2210736")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "171 ORE => 8 CNZTR",
                    "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
                    "114 ORE => 4 BHXH",
                    "14 VRPVC => 6 BMBT",
                    "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
                    "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
                    "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
                    "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
                    "5 BMBT => 4 WPTQ",
                    "189 ORE => 9 KTJDG",
                    "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
                    "12 VRPVC, 27 CNZTR => 2 XDBXC",
                    "15 KTJDG, 12 BHXH => 5 XCVML",
                    "3 BHXH, 2 VRPVC => 7 MZWV",
                    "121 ORE => 7 VRPVC",
                    "7 XCVML => 6 RJRHP",
                    "5 BHXH, 4 VRPVC => 5 LTCX"
                }, "460664")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var parser = new InputParser(input);
            var graph = parser.ParseDependencyGraph();
            var recipes = parser.ParseRecipes();

            var resolver = new IngredientResolver(recipes, graph);
            var fuel = new Dictionary<string, long>() { { "FUEL", 1 } };
            var result = resolver.ResolveToBaseIngredients(fuel);

            return result["ORE"].ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var total = 1000000000000;

            var parser = new InputParser(input);
            var graph = parser.ParseDependencyGraph();
            var recipes = parser.ParseRecipes();

            var resolver = new IngredientResolver(recipes, graph);
            var fuel = new Dictionary<string, long>() { { "FUEL", 1 } };
            var result = resolver.ResolveToBaseIngredients(fuel);

            var low = total / result["ORE"];
            var high = 2 * low;
            var bestFactor = BinarySearch(low, high, f =>
            {
                var fuel = new Dictionary<string, long>() { { "FUEL", f } };
                var result = resolver.ResolveToBaseIngredients(fuel);

                return result["ORE"] <= total;
            });

            return bestFactor.ToString();
        }

        private long BinarySearch(long low, long high, Predicate<long> predicate)
        {
            while (low < high)
            {
                var mid = low + (high - low) / 2;
                if (predicate(mid))
                {
                    low = mid + 1;
                }
                else
                {
                    high = mid;
                }
            }

            if (predicate(low))
            {
                return low;
            }
            else
            {
                return low - 1;
            }
        }
    }
}
