﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day14
{
    internal class InputParser
    {
        private readonly Dictionary<string, List<string>> neighbours = new Dictionary<string, List<string>>();
        private readonly IEnumerable<string> input;

        public InputParser(IEnumerable<string> input)
        {
            this.input = input;
        }

        public DependencyGraph ParseDependencyGraph()
        {
            var nodes = new HashSet<string>();
            foreach (var line in input)
            {
                var parts = line.Split('=');
                var sources = ParseSources(parts[0].Trim(' '));
                var target = ParseAmount(parts[1].Trim(' ', '>'));

                nodes.Add(target.Name);
                foreach(var source in sources)
                {
                    nodes.Add(source.Name);
                    AddNeighbour(source.Name, target.Name);
                }
            }

            return new DependencyGraph(nodes, neighbours.ToDictionary<KeyValuePair<string, List<string>>, string, IReadOnlyCollection<string>>(kvp => kvp.Key, kvp => kvp.Value));
        }

        public Recipes ParseRecipes()
        {
            var transformations = new List<Transformation>();
            foreach (var line in input)
            {
                var parts = line.Split('=');
                var sources = ParseSources(parts[0].Trim(' '));
                var target = ParseAmount(parts[1].Trim(' ', '>'));

                var transformation = new Transformation(target.Name, target.Amount, sources.ToDictionary(s => s.Name, s => s.Amount));
                transformations.Add(transformation);
            }

            return new Recipes(transformations);
        }

        private (string Name, long Amount) ParseAmount(string token)
        {
            var parts = token.Split(' ');
            var amount = long.Parse(parts[0]);
            var name = parts[1];
            return (name, amount);
        }

        private IEnumerable<(string Name, long Amount)> ParseSources(string token)
        {
            var names = new List<(string, long)>();
            foreach(var part in token.Split(',').Select(p => p.Trim(' ')))
            {
                names.Add(ParseAmount(part));
            }

            return names;
        }

        private void AddNeighbour(string node, string neighbour)
        {
            if (!neighbours.ContainsKey(node))
            {
                neighbours[node] = new List<string>();
            }
            neighbours[node].Add(neighbour);
        }
    }
}
