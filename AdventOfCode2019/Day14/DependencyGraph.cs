﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day14
{
    internal class DependencyGraph
    {
        private readonly Dictionary<string, IReadOnlyCollection<string>> neighbours;

        public DependencyGraph(IEnumerable<string> nodes, Dictionary<string, IReadOnlyCollection<string>> neighbours)
        {
            Nodes = nodes;
            this.neighbours = neighbours;
        }

        public IEnumerable<string> Nodes { get; }

        public IEnumerable<string> GetNextNodes(string node)
        {
            return neighbours.ContainsKey(node) ? neighbours[node] : Enumerable.Empty<string>();
        }
    }
}
