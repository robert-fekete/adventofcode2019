﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day14
{
    internal class TopologicalSortingCalculator
    {
        private readonly Dictionary<string, bool> temporaryMarking = new Dictionary<string, bool>();
        private readonly Dictionary<string, bool> permanentMarking = new Dictionary<string, bool>();

        private readonly List<string> sorting = new List<string>();
        private readonly DependencyGraph graph;

        public TopologicalSortingCalculator(DependencyGraph graph)
        {
            this.graph = graph;
        }

        public IEnumerable<string> GetTopologicalSorting()
        {
            temporaryMarking.Clear();
            permanentMarking.Clear();
            sorting.Clear();

            foreach(var node in graph.Nodes)
            {
                if (!IsPermanentMarked(node))
                {
                    DfsVisitRec(node);
                }
            }

            return sorting;
        }
        private void DfsVisitRec(string node)
        {
            if (IsPermanentMarked(node))
            {
                return;
            }
            if (IsTemporaryMarked(node))
            {
                throw new InvalidOperationException("Graph is not acyclic!");
            }

            temporaryMarking[node] = true;
            foreach (var nextNode in graph.GetNextNodes(node))
            {
                DfsVisitRec(nextNode);
            }
            temporaryMarking[node] = false;
            permanentMarking[node] = true;

            sorting.Add(node);
        }

        private bool IsPermanentMarked(string node)
        {
            return permanentMarking.ContainsKey(node) && permanentMarking[node];
        }

        private bool IsTemporaryMarked(string node)
        {
            return temporaryMarking.ContainsKey(node) && temporaryMarking[node];
        }

    }
}
