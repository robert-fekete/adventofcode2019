﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day14
{
    internal class Transformation
    {
        private readonly long targetAmount;
        private readonly Dictionary<string, long> sources;

        public Transformation(string name, long amount, Dictionary<string, long> sources)
        {
            targetAmount = amount;
            Name = name;
            this.sources = sources;
        }

        public string Name { get; }

        public Dictionary<string, long> GetourcesForAmount(long amount)
        {
            var factor = (long)Math.Ceiling((decimal)amount / targetAmount);

            return sources.ToDictionary(kvp => kvp.Key, kvp => kvp.Value * factor);
        }
    }
}
