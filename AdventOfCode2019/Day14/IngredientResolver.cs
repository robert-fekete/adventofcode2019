﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day14
{
    internal class IngredientResolver
    {
        private readonly Recipes recipes;
        private readonly DependencyGraph graph;

        public IngredientResolver(Recipes recipes, DependencyGraph graph)
        {
            this.recipes = recipes;
            this.graph = graph;
        }

        public Dictionary<string, long> ResolveToBaseIngredients(Dictionary<string, long> ingredients)
        {
            var sorter = new TopologicalSortingCalculator(graph);
            var sorting = sorter.GetTopologicalSorting();

            foreach(var name in sorting.SkipLast(1))
            {
                var amount = ingredients[name];
                var sources = recipes.GetSources(name, amount);
                foreach(var kvp in sources)
                {
                    IncrementIngredient(kvp.Key, kvp.Value, ingredients);
                }

                ingredients[name] = 0;
            }

            return ingredients;
        }

        public void IncrementIngredient(string name, long amount, Dictionary<string, long> ingredients)
        {
            if (ingredients.ContainsKey(name))
            {
                ingredients[name] += amount;
            }
            else
            {
                ingredients[name] = amount;
            }
        }
    }
}
