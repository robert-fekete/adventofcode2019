﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day14
{
    internal class Recipes
    {
        private readonly IEnumerable<Transformation> transformations;

        public Recipes(IEnumerable<Transformation> transformations)
        {
            this.transformations = transformations;
        }

        internal Dictionary<string, long> GetSources(string name, long amount)
        {
            var transformation = transformations.First(t => t.Name == name);
            var sources = transformation.GetourcesForAmount(amount);

            return sources;
        }
    }
}