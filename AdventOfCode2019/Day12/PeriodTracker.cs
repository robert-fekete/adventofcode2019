﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day12
{
    internal class PeriodTracker
    {
        private readonly Dictionary<string, long> periods = new Dictionary<string, long>();
        private readonly Dictionary<string, List<int>> sequences = new Dictionary<string, List<int>>();

        public void TrackSubPeriod(string name, int value)
        {
            if (!sequences.ContainsKey(name))
            {
                sequences[name] = new List<int>();
            }
            sequences[name].Add(value);
        }

        public bool IsFinished()
        {
            return sequences.Keys.Any() && sequences.Keys.All(periods.ContainsKey);
        }

        public long GetPeriod(LowestCommonMultipleCalculator calculator)
        {
            return calculator.CalculateLcm(sequences.Keys.Select(name => periods[name]));
        }

        public void Update()
        {
            foreach(var name in sequences.Keys)
            {
                if (periods.ContainsKey(name))
                {
                    continue;
                }

                var period = GetPeriod(name);
                if (period != -1)
                {
                    periods[name] = period;
                }
            }
        }

        private long GetPeriod(string name)
        {
            var sequence = sequences[name];
            if (sequence.Count <= 2 || sequence.Count % 2 != 0)
            {
                return -1;
            }

            var half = sequence.Count / 2;
            for (int i = 0; i < half; i++)
            {
                if (sequence[i] != sequence[half + i])
                {
                    return -1;
                }
            }

            return half;
        }
    }
}