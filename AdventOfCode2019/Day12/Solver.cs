﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day12
{
    internal class Solver : ISolver
    {
        public int DayNumber => 12;

        public string FirstExpected => "6227";

        public string SecondExpected => "331346071640472"; // 67293 < x

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "<x=-1, y=0, z=2>",
                    "<x=2, y=-10, z=-7>",
                    "<x=4, y=-8, z=8>",
                    "<x=3, y=5, z=-1>"
                }, 10, 179)
                .AddTest((i, p) => First(i, p))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "<x=-1, y=0, z=2>",
                    "<x=2, y=-10, z=-7>",
                    "<x=4, y=-8, z=8>",
                    "<x=3, y=5, z=-1>"
                }, "2772")
                .AddTest(ExecuteSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input, 1000).ToString();
        }

        private int First(IEnumerable<string> input, int ticks)
        {
            var moons = MoonCollection.FromInput(input);
            moons.Simulate(ticks);

            return moons.TotalEnergy;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var moons = MoonCollection.FromInput(input);

            var period = moons.FindPeriod();

            return period.ToString();
        }
    }
}
