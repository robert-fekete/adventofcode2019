﻿using System;

namespace AdventOfCode2019.Day12
{
    internal class Vector3
    {
        public Vector3(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public int X { get; }
        public int Y { get; }
        public int Z { get; }

        public int Rank => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);

        public Vector3 GetGravityEffect(Vector3 other)
        {
            var dx = GetGravityEffect(X, other.X);
            var dy = GetGravityEffect(Y, other.Y);
            var dz = GetGravityEffect(Z, other.Z);

            return new Vector3(dx, dy, dz);
        }

        public static Vector3 operator+(Vector3 a, Vector3 b)
        {
            return new Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        private int GetGravityEffect(int own, int other)
        {
            if (own == other)
            {
                return 0;
            }
            else
            {
                return own > other ? -1 : 1;
            }
        }

        public override string ToString()
        {
            return $"<x={X}, y={Y}, z={Z}>";
        }
    }
}
