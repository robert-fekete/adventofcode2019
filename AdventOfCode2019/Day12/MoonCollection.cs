﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day12
{
    internal class MoonCollection
    {
        private readonly Dictionary<Moon, IEnumerable<Moon>> grouping;

        private MoonCollection(IEnumerable<Moon> moons)
        {
            grouping = moons.ToDictionary(m => m, m => moons.Where(m2 => !m2.Equals(m)));
        }

        private IEnumerable<Moon> Moons => grouping.Keys;
        public int TotalEnergy => Moons.Sum(m => m.Energy);
        public void Simulate(int ticks)
        {
            for(int t = 0; t < ticks; t++)
            {
                foreach(var moon in Moons)
                {
                    var remainingMoons = grouping[moon];
                    moon.UpdateVelocity(remainingMoons);
                }

                foreach(var moon in Moons)
                {
                    moon.UpdatePosition();
                    //System.Console.WriteLine(moon.ToString());
                }
                //System.Console.WriteLine();
            }
        }

        public long FindPeriod()
        {
            var trackers = grouping.Keys.ToDictionary(k => k, _ => new PeriodTracker());
            while (!trackers.Values.All(t => t.IsFinished()))
            {
                Simulate(1);
                foreach(var moon in grouping.Keys)
                {
                    moon.TrackPeriod(trackers[moon]);
                    trackers[moon].Update();
                }
            }

            var calculator = new LowestCommonMultipleCalculator();
            var lme = calculator.CalculateLcm(trackers.Values.Select(t => t.GetPeriod(calculator)));

            return lme;
        }

        public static MoonCollection FromInput(IEnumerable<string> input)
        {
            var moons = new List<Moon>();
            foreach(var line in input)
            {
                var moon = ParseLine(line);
                moons.Add(moon);
            }

            return new MoonCollection(moons);
        }

        private static Moon ParseLine(string line)
        {
            var coordinates = line.Split(' ').Select(p => p.Trim(',', '<', '>'));
            var values = coordinates.Select(p => p.Split('=')[1]).Select(int.Parse);

            var x = values.First();
            var y = values.Skip(1).First();
            var z = values.Skip(2).First();

            return new Moon(new Vector3(x, y, z));
        }
    }
}
