﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day12
{
    internal class LowestCommonMultipleCalculator
    {
        public long CalculateLcm(params long[] numbers)
        {
            return CalculateLcm(numbers as IEnumerable<long>);
        }
        public long CalculateLcm(IEnumerable<long> numbers)
        {
            var acc = numbers.First();
            foreach(var next in numbers.Skip(1))
            {
                acc = CalculateLcm(acc, next);
            }

            return acc;
        }

        private long CalculateLcm(long a, long b)
        {
            var gca = CalculateGcd(a, b);
            return a / gca * b;
        }

        private long CalculateGcd(long a, long b)
        {
            var current = Math.Min(a, b);
            while(!((a % current == 0) && (b % current == 0)))
            {
                current--;
            }

            return current;
        }
    }
}