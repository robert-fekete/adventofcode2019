﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day12
{
    internal class Moon
    {
        private Vector3 position;
        private Vector3 velocity = new Vector3(0, 0, 0);

        public Moon(Vector3 position)
        {
            this.position = position;
        }

        public int Energy => position.Rank * velocity.Rank;

        public void UpdateVelocity(IEnumerable<Moon> remainingMoons)
        {
            foreach(var other in remainingMoons)
            {
                var gravitatyEffect = position.GetGravityEffect(other.position);
                velocity += gravitatyEffect;
            }
        }

        public void UpdatePosition()
        {
            position += velocity;
        }

        public override string ToString()
        {
            return $"pos={position}, vel={velocity}";
        }

        public void TrackPeriod(PeriodTracker tracker)
        {
            tracker.TrackSubPeriod("X", velocity.X);
            tracker.TrackSubPeriod("Y", velocity.Y);
            tracker.TrackSubPeriod("Z", velocity.Z);
        }
    }
}
