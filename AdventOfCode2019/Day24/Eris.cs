﻿using System;

namespace AdventOfCode2019.Day23
{
    internal class Eris
    {
        private readonly bool[][] map;

        public Eris(bool[][] map)
        {
            this.map = map;
        }

        public Eris GetNextGeneration()
        {
            var newMap = new bool[5][];
            for (int y = 0; y < 5; y++)
            {
                newMap[y] = new bool[5];
                for (int x = 0; x < 5; x++)
                {
                    var count = GetNeighbouringBugs(x, y);
                    if(map[y][x] && count != 1)
                    {
                        newMap[y][x] = false;
                    }
                    else if(!map[y][x] && (count == 1 || count == 2))
                    {
                        newMap[y][x] = true;
                    }
                    else
                    {
                        newMap[y][x] = map[y][x];
                    }
                }
            }

            return new Eris(newMap);
        }

        public long GetBioDiversity()
        {
            var bioDiversity = 0L;
            var index = 0;
            for(int y = 0; y < 5; y++)
            {
                for (int x = 0; x < 5; x++)
                {
                    if (map[y][x])
                    {
                        var value = 1L << index;
                        bioDiversity += value;
                    }
                    index++;
                }
            }

            return bioDiversity;
        }

        private int GetNeighbouringBugs(int x, int y)
        {
            var offsets = new[]
            {
                (0, 1),
                (1, 0),
                (0, -1),
                (-1, 0)
            };

            var total = 0;
            foreach((var dx, var dy) in offsets)
            {
                var targetX = x + dx;
                var targetY = y + dy;
                if (targetX < 0 || targetX >= 5 || targetY < 0 || targetY >= 5)
                {
                    continue;
                }

                if (map[targetY][targetX])
                {
                    total++;
                }
            }

            return total;
        }

        public void Print()
        {
            for(int y = 0; y < 5; y++)
            {
                for(int x = 0; x < 5; x++)
                {
                    Console.Write(map[y][x] ? '#' : '.');
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
