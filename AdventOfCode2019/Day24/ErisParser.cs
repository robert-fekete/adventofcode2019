﻿using AdventOfCode2019.Day23;
using System.Collections.Generic;

namespace AdventOfCode2019.Day24
{
    internal class ErisParser
    {
        public Eris ParseFirst(IEnumerable<string> input)
        {
            return new Eris(ParseMap(input));
        }
        public RecursiveEris ParseSecond(IEnumerable<string> input)
        {
            return new RecursiveEris(ParseMap(input));
        }

        private static bool[][] ParseMap(IEnumerable<string> input)
        {
            var map = new List<bool[]>();
            foreach (var line in input)
            {
                var row = new bool[5];
                for (int x = 0; x < 5; x++)
                {
                    row[x] = line[x] == '#';
                }
                map.Add(row);
            }

            return map.ToArray();
        }
    }
}
