﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace AdventOfCode2019.Day23
{
    internal class RecursiveEris
    {
        private Dictionary<int, bool[][]> maps = new Dictionary<int, bool[][]>();
        private readonly bool[][] EmptyMap;

        public RecursiveEris(bool[][] map)
        {
            maps[0] = map;
            EmptyMap = new bool[][]
            {
                new bool[5],
                new bool[5],
                new bool[5],
                new bool[5],
                new bool[5],
            };
        }

        public int TotalBugs => maps.Sum(kvp => kvp.Value
                                                  .Sum(r => r.Sum(b => (b ? 1 : 0))));

        public void ProgressGeneration(int level)
        {
            var newMap = new Dictionary<int, bool[][]>();
            for(int l = -level; l <= level; l++)
            {
                newMap[l] = GetNextLevel(l);
            }

            maps = newMap;
        }

        private bool[][] GetNextLevel(int level)
        {
            var previousMap = GetMap(level);

            var newMap = new bool[5][];
            for (int y = 0; y < 5; y++)
            {
                newMap[y] = new bool[5];
                for (int x = 0; x < 5; x++)
                {
                    if (y == 2 && x == 2)
                    {
                        continue;
                    }

                    var count = GetNeighbouringBugs(level, x, y);
                    if (previousMap[y][x] && count != 1)
                    {
                        newMap[y][x] = false;
                    }
                    else if (!previousMap[y][x] && (count == 1 || count == 2))
                    {
                        newMap[y][x] = true;
                    }
                    else
                    {
                        newMap[y][x] = previousMap[y][x];
                    }
                }
            }

            return newMap;
        }

        private bool[][] GetMap(int level)
        {
            if (maps.ContainsKey(level))
            {
                return maps[level];
            }
            else
            {
                return  EmptyMap;
            }
        }

        private int GetNeighbouringBugs(int level, int x, int y)
        {
            var offsets = new[]
            {
                (0, 1),
                (1, 0),
                (0, -1),
                (-1, 0)
            };

            var total = 0;
            foreach((var dx, var dy) in offsets)
            {
                var targetX = x + dx;
                var targetY = y + dy;
                if (targetX < 0 || targetX >= 5 || targetY < 0 || targetY >= 5)
                {
                    total += GetOuterBugs(level - 1, targetX, targetY);
                }
                else if (targetY == 2 && targetX == 2)
                {
                    total += GetInnerBugs(level + 1, x, y);
                }
                else if (maps.ContainsKey(level) && maps[level][targetY][targetX])
                {
                    total++;
                }
            }

            return total;
        }

        private int GetOuterBugs(int level, int x, int y)
        {
            var map = GetMap(level);
            if (x == -1)
            {
                return map[2][1] ? 1 : 0;
            }
            else if (y == -1)
            {
                return map[1][2] ? 1 : 0;
            }
            else if (x == 5)
            {
                return map[2][3] ? 1 : 0;
            }
            else if (y == 5)
            {
                return map[3][2] ? 1 : 0;
            }
            else
            {
                throw new InvalidOperationException("Invalid coordinates for outer bugs");
            }
        }

        private int GetInnerBugs(int level, int x, int y)
        {
            var map = GetMap(level);
            if (x == 1 && y == 2)
            {
                return map.Select(l => l[0]).Sum(b => b ? 1 : 0);
            }
            else if(x == 2 && y == 1)
            {
                return map[0].Sum(b => b ? 1 : 0);
            }
            else if (x == 3 && y == 2)
            {
                return map.Select(l => l[4]).Sum(b => b ? 1 : 0);
            }
            else if(x == 2 && y == 3)
            {
                return map[4].Sum(b => b ? 1 : 0);
            }
            else
            {
                throw new InvalidOperationException("Invalid coordinates for inner bugs");
            }
        }

        public void Print()
        {
            for (int level = -5; level <= 5; level++)
            {
                Console.WriteLine($"Level {level}:");
                for (int y = 0; y < 5; y++)
                {
                    for (int x = 0; x < 5; x++)
                    {
                        Console.Write(maps[level][y][x] ? '#' : '.');
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
