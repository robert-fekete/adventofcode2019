﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;

namespace AdventOfCode2019.Day24
{
    internal class Solver : ISolver
    {
        public int DayNumber => 24;

        public string FirstExpected => "28903899";

        public string SecondExpected => "1896";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "....#",
                    "#..#.",
                    "#..##",
                    "..#..",
                    "#....",
                }, "2129920")
                .AddTest(ExecuteFirst)
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "....#",
                    "#..#.",
                    "#..##",
                    "..#..",
                    "#....",
                }, 10, 99)
                .AddTest(SolveSecond)
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var parser = new ErisParser();
            var eris = parser.ParseFirst(input);
            var history = new HashSet<long>() { eris.GetBioDiversity() };

            while (true)
            {
                eris = eris.GetNextGeneration();
                
                var bioDiversity = eris.GetBioDiversity();
                if (history.Contains(bioDiversity))
                {
                    return bioDiversity.ToString();
                }
                history.Add(bioDiversity);
            }
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return SolveSecond(input, 200).ToString();
        }

        private int SolveSecond(IEnumerable<string> input, int iterations)
        {
            var parser = new ErisParser();
            var eris = parser.ParseSecond(input);

            for (int i = 0; i < iterations; i++)
            {
                eris.ProgressGeneration(i + 1);
            }

            return eris.TotalBugs;
        }
    }
}
