﻿namespace AdventOfCode2019.Day20
{
    internal interface IPortalParser
    {
        string GetKey(int x, int y);
        (int, int, int) GetPortal(int x, int y);
        bool IsFinished(int x, int y);
        (int, int) GetNextCoordinates(int x, int y);
        (int, int) GetStartPosition();
        bool IsPortal(int x, int y);
    }
}
