﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day20
{
    internal class Graph
    {
        private readonly (int Dx, int Dy)[] offsets = new[]
        {
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0)
        };

        private string[] map;
        private Dictionary<(int, int), (int, int, int)> portals;

        public Graph(string[] map, Dictionary<(int, int), (int, int, int)> portals)
        {
            this.map = map;
            this.portals = portals;
        }

        internal IEnumerable<(int, int, int)> GetNeighbours((int X, int Y) current)
        {
            var nexts = new List<(int, int, int)>();
            foreach ((var dx, var dy) in offsets)
            {
                var x = current.X + dx;
                var y = current.Y + dy;

                if (x < 2 || x >= map[2].Length - 2) // 2 and -2 for the portals around the map
                {
                    continue;
                }
                if (y < 2 || y >= map.Length - 2) // 2 and -2 for the portals around the map
                {
                    continue;
                }

                if (map[y][x] == '.')
                {
                    nexts.Add((x, y, 0));
                }
            }
            if (portals.ContainsKey(current))
            {
                nexts.Add(portals[current]);
            }

            return nexts;
        }
    }
}
