﻿namespace AdventOfCode2019.Day20.Parsers
{
    internal class OuterLeftParser : IPortalParser
    {
        private readonly string[] input;

        public OuterLeftParser(string[] input)
        {
            this.input = input;
        }
        public string GetKey(int x, int y)
        {
            return new string(new[] { input[y][x - 1], input[y][x] });
        }

        public (int, int) GetNextCoordinates(int x, int y)
        {
            return (x, y + 1);
        }

        public (int, int, int) GetPortal(int x, int y)
        {
            return (x + 1, y, 1);
        }

        public (int, int) GetStartPosition()
        {
            return (1, 2);
        }

        public bool IsFinished(int x, int y)
        {
            return input[y][x + 1] == ' ';
        }

        public bool IsPortal(int x, int y)
        {
            (var px, var py, var _) = GetPortal(x, y);
            return input[py][px] == '.';
        }
    }
}
