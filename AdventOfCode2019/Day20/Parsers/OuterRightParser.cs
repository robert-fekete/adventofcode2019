﻿namespace AdventOfCode2019.Day20.Parsers
{
    internal class OuterRightParser : IPortalParser
    {
        private readonly string[] input;

        public OuterRightParser(string[] input)
        {
            this.input = input;
        }
        public string GetKey(int x, int y)
        {
            return new string(new[] { input[y][x], input[y][x + 1] });
        }

        public (int, int) GetNextCoordinates(int x, int y)
        {
            return (x, y + 1);
        }

        public (int, int, int) GetPortal(int x, int y)
        {
            return (x - 1, y, 1);
        }

        public (int, int) GetStartPosition()
        {
            var length = input[2].Length;
            return (length - 2, 2);
        }

        public bool IsFinished(int x, int y)
        {
            return x >= input[y].Length ||  input[y][x - 1] == ' ';
        }

        public bool IsPortal(int x, int y)
        {
            (var px, var py, var _) = GetPortal(x, y);
            return input[py][px] == '.';
        }
    }
}
