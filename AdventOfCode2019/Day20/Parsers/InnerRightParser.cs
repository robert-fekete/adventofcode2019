﻿namespace AdventOfCode2019.Day20.Parsers
{
    internal class InnerRightParser : IPortalParser
    {
        private readonly string[] input;
        private readonly int top;
        private readonly int left;

        public InnerRightParser(string[] input, (int, int) innerCorner)
        {
            this.input = input;
            (left, top) = innerCorner;
        }

        public string GetKey(int x, int y)
        {
            return new string(new[] { input[y][x - 1], input[y][x] });
        }

        public (int, int) GetNextCoordinates(int x, int y)
        {
            return (x, y + 1);
        }

        public (int, int, int) GetPortal(int x, int y)
        {
            return (x + 1, y, -1);
        }

        public (int, int) GetStartPosition()
        {
            var right = input[top].Length - 1 - left;
            return (right, top);
        }

        public bool IsFinished(int x, int y)
        {
            return input[y][x] == '#' || input[y][x] == '.';
        }

        public bool IsPortal(int x, int y)
        {
            (var px, var py, var _) = GetPortal(x, y);
            return input[py][px] == '.';
        }
    }
}
