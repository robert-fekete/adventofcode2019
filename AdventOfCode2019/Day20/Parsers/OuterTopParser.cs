﻿namespace AdventOfCode2019.Day20.Parsers
{
    internal class OuterTopParser : IPortalParser
    {
        private readonly string[] input;

        public OuterTopParser(string[] input)
        {
            this.input = input;
        }
        public string GetKey(int x, int y)
        {
            return new string(new[] { input[y - 1][x], input[y][x] });
        }

        public (int, int) GetNextCoordinates(int x, int y)
        {
            return (x + 1, y);
        }

        public (int, int, int) GetPortal(int x, int y)
        {
            return (x, y + 1, 1);
        }

        public (int, int) GetStartPosition()
        {
            return (2, 1);
        }

        public bool IsFinished(int x, int y)
        {
            return x >= input[y].Length || input[y + 1][x] == ' ';
        }

        public bool IsPortal(int x, int y)
        {
            (var px, var py, var _) = GetPortal(x, y);
            return input[py][px] == '.';
        }
    }
}
