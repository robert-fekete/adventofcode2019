﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day20
{
    internal class MultiLeveLTraversal
    {
        public int Traverse(Graph graph, (int , int, int) source, (int, int, int) target)
        {
            var backlog = new Queue<((int X, int Y, int Z), int)>();
            backlog.Enqueue((source, 0));
            var visited = new HashSet<(int, int, int)>();

            while (backlog.Any())
            {
                (var current, var depth) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (current == target)
                {
                    return depth;
                }

                foreach ((var x, var y, var dz) in graph.GetNeighbours((current.X, current.Y)))
                {
                    if (current.Z == 0 && dz == 1)
                    {
                        continue; // Outer portals doesn't function on the top most level
                    }
                    backlog.Enqueue(((x, y, current.Z + dz), depth + 1));
                }
            }

            throw new InvalidOperationException("Couldn't find target");
        }
    }
}