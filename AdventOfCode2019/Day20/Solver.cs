﻿using AdventOfCode.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day20
{
    internal class Solver : ISolver
    {
        public int DayNumber => 20;

        public string FirstExpected => "628";

        public string SecondExpected => "7506";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "         A",
                    "         A",
                    "  #######.#########  ",
                    "  #######.........#  ",
                    "  #######.#######.#  ",
                    "  #######.#######.#  ",
                    "  #######.#######.#  ",
                    "  #####  B    ###.#  ",
                    "BC...##  C    ###.#  ",
                    "  ##.##       ###.#  ",
                    "  ##...DE  F  ###.#  ",
                    "  #####    G  ###.#  ",
                    "  #########.#####.#  ",
                    "DE..#######...###.#  ",
                    "  #.#########.###.#  ",
                    "FG..#########.....#  ",
                    "  ###########.#####  ",
                    "             Z",
                    "             Z",
                }, 23)
                .AddTestCase(new[]
                {
                    "                   A",
                    "                   A",
                    "  #################.#############  ",
                    "  #.#...#...................#.#.#  ",
                    "  #.#.#.###.###.###.#########.#.#  ",
                    "  #.#.#.......#...#.....#.#.#...#  ",
                    "  #.#########.###.#####.#.#.###.#  ",
                    "  #.............#.#.....#.......#  ",
                    "  ###.###########.###.#####.#.#.#  ",
                    "  #.....#        A   C    #.#.#.#  ",
                    "  #######        S   P    #####.#  ",
                    "  #.#...#                 #......VT",
                    "  #.#.#.#                 #.#####  ",
                    "  #...#.#               YN....#.#  ",
                    "  #.###.#                 #####.#  ",
                    "DI....#.#                 #.....#  ",
                    "  #####.#                 #.###.#  ",
                    "ZZ......#               QG....#..AS",
                    "  ###.###                 #######  ",
                    "JO..#.#.#                 #.....#  ",
                    "  #.#.#.#                 ###.#.#  ",
                    "  #...#..DI             BU....#..LF",
                    "  #####.#                 #.#####  ",
                    "YN......#               VT..#....QG",
                    "  #.###.#                 #.###.#  ",
                    "  #.#...#                 #.....#  ",
                    "  ###.###    J L     J    #.#.###  ",
                    "  #.....#    O F     P    #.#...#  ",
                    "  #.###.#####.#.#####.#####.###.#  ",
                    "  #...#.#.#...#.....#.....#.#...#  ",
                    "  #.#####.###.###.#.#.#########.#  ",
                    "  #...#.#.....#...#.#.#.#.....#.#  ",
                    "  #.###.#####.###.###.#.#.#######  ",
                    "  #.#.........#...#.............#  ",
                    "  #########.###.###.#############  ",
                    "           B   J   C",
                    "           U   P   P",
                }, 58)
                .AddTest(i => SolveFirst(i))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(new[]
                {
                    "         A",
                    "         A",
                    "  #######.#########  ",
                    "  #######.........#  ",
                    "  #######.#######.#  ",
                    "  #######.#######.#  ",
                    "  #######.#######.#  ",
                    "  #####  B    ###.#  ",
                    "BC...##  C    ###.#  ",
                    "  ##.##       ###.#  ",
                    "  ##...DE  F  ###.#  ",
                    "  #####    G  ###.#  ",
                    "  #########.#####.#  ",
                    "DE..#######...###.#  ",
                    "  #.#########.###.#  ",
                    "FG..#########.....#  ",
                    "  ###########.#####  ",
                    "             Z",
                    "             Z",
                }, 26)
                .AddTestCase(new[]
                {
                    "             Z L X W       C",
                    "             Z P Q B       K",
                    "  ###########.#.#.#.#######.###############  ",
                    "  #...#.......#.#.......#.#.......#.#.#...#  ",
                    "  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  ",
                    "  #.#...#.#.#...#.#.#...#...#...#.#.......#  ",
                    "  #.###.#######.###.###.#.###.###.#.#######  ",
                    "  #...#.......#.#...#...#.............#...#  ",
                    "  #.#########.#######.#.#######.#######.###  ",
                    "  #...#.#    F       R I       Z    #.#.#.#  ",
                    "  #.###.#    D       E C       H    #.#.#.#  ",
                    "  #.#...#                           #...#.#  ",
                    "  #.###.#                           #.###.#  ",
                    "  #.#....OA                       WB..#.#..ZH",
                    "  #.###.#                           #.#.#.#  ",
                    "CJ......#                           #.....#  ",
                    "  #######                           #######  ",
                    "  #.#....CK                         #......IC",
                    "  #.###.#                           #.###.#  ",
                    "  #.....#                           #...#.#  ",
                    "  ###.###                           #.#.#.#  ",
                    "XF....#.#                         RF..#.#.#  ",
                    "  #####.#                           #######  ",
                    "  #......CJ                       NM..#...#  ",
                    "  ###.#.#                           #.###.#  ",
                    "RE....#.#                           #......RF",
                    "  ###.###        X   X       L      #.#.#.#  ",
                    "  #.....#        F   Q       P      #.#.#.#  ",
                    "  ###.###########.###.#######.#########.###  ",
                    "  #.....#...#.....#.......#...#.....#.#...#  ",
                    "  #####.#.###.#######.#######.###.###.#.#.#  ",
                    "  #.......#.......#.#.#.#.#...#...#...#.#.#  ",
                    "  #####.###.#####.#.#.#.#.###.###.#.###.###  ",
                    "  #.......#.....#.#...#...............#...#  ",
                    "  #############.#.#.###.###################  ",
                    "               A O F   N",
                    "               A A D   M",
                }, 396)
                .AddTest(i => SolveSecond(i))
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return SolveFirst(input).ToString();
        }

        private int SolveFirst(IEnumerable<string> input)
        {
            var parser = new GraphParser();
            (var graph, var source, var target) = parser.Parse(input);

            var traversal = new StandardTraversal();
            (var sx, var sy, var _) = source;
            (var tx, var ty, var _) = target;
            var path = traversal.Traverse(graph, (sx, sy), (tx, ty));
            return path;
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return SolveSecond(input).ToString();
        }

        private int SolveSecond(IEnumerable<string> input)
        {
            var parser = new GraphParser();
            (var graph, var source, var target) = parser.Parse(input);

            var traversal = new MultiLeveLTraversal();
            var path = traversal.Traverse(graph, source, target);
            return path;
        }
    }
}
