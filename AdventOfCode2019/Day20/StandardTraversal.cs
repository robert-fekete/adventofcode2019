﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day20
{
    internal class StandardTraversal
    {
        public int Traverse(Graph graph, (int, int) source, (int, int) target)
        {
            var backlog = new Queue<((int, int), int)>();
            backlog.Enqueue((source, 0));
            var visited = new HashSet<(int, int)>();

            while (backlog.Any())
            {
                (var current, var depth) = backlog.Dequeue();

                if (visited.Contains(current))
                {
                    continue;
                }
                visited.Add(current);

                if (current == target)
                {
                    return depth;
                }

                foreach ((var x, var y, var _) in graph.GetNeighbours(current))
                {
                    backlog.Enqueue(((x, y), depth + 1));
                }
            }

            throw new InvalidOperationException("Couldn't find target");
        }
    }
}
