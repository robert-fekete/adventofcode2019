﻿using AdventOfCode2019.Day20.Parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day20
{
    internal class GraphParser
    {
        internal (Graph, (int, int, int), (int, int, int)) Parse(IEnumerable<string> input)
        {
            var lines = input.ToArray();

            (var portals, var source, var target) = GetPortals(input.ToArray());

            return (new Graph(lines, portals), source, target);
        }

        private (Dictionary<(int, int), (int, int, int)>, (int, int, int), (int, int, int)) GetPortals(string[] input)
        {
            var temp = new Dictionary<string, List<(int, int, int)>>();

            GetOuterPortals(input, temp);
            GetInnerPortals(input, temp);

            var portals = new Dictionary<(int, int), (int, int, int)>();
            foreach(var kvp in temp)
            {
                if (kvp.Value.Count > 2)
                {
                    throw new InvalidOperationException($"More than 2 portals of type {kvp.Key}");
                }
                if (kvp.Key == "AA" || kvp.Key == "ZZ")
                {
                    continue;
                }
                (var ax, var ay, var az) = kvp.Value[0];
                (var bx, var by, var bz) = kvp.Value[1];
                portals[(ax, ay)] = (bx, by, az); // Switching Z coordinate: outer portal makes the destination being upper and vice versa
                portals[(bx, by)] = (ax, ay, bz); // Switching Z coordinate: outer portal makes the destination being upper and vice versa
            }

            (var sx, var sy, var _) = temp["AA"][0]; // Z doesn't matter for source
            (var tx, var ty, var _) = temp["ZZ"][0]; // Z doesn't matter for target

            return (portals, (sx, sy, 0), (tx, ty, 0));
        }

        private void GetOuterPortals(string[] input, Dictionary<string, List<(int, int, int)>> temp)
        {
            CollectPortals(new OuterTopParser(input), temp);
            CollectPortals(new OuterBottomParser(input), temp);
            CollectPortals(new OuterLeftParser(input), temp);
            CollectPortals(new OuterRightParser(input), temp);
        }

        private void GetInnerPortals(string[] input, Dictionary<string, List<(int, int, int)>> temp)
        {
            var corner = GetInnerCorner(input);
            CollectPortals(new InnerTopParser(input, corner), temp);
            CollectPortals(new InnerBottomParser(input, corner), temp);
            CollectPortals(new InnerLeftParser(input, corner), temp);
            CollectPortals(new InnerRightParser(input, corner), temp);
        }

        private (int, int) GetInnerCorner(string[] input)
        {
            var i = 2;
            while (input[i][i] != ' ' && !char.IsLetter(input[i][i]))
            {
                i++;
            }
            return (i, i);
        }

        private void CollectPortals(IPortalParser portalParser, Dictionary<string, List<(int, int, int)>> temp)
        {
            (var x, var y) = portalParser.GetStartPosition();

            while (!portalParser.IsFinished(x, y))
            {
                if (portalParser.IsPortal(x, y))
                {
                    var key = portalParser.GetKey(x, y);
                    if (!temp.ContainsKey(key))
                    {
                        temp[key] = new List<(int, int, int)>();
                    }
                    temp[key].Add(portalParser.GetPortal(x, y));
                }
                (x, y) = portalParser.GetNextCoordinates(x, y);
            }

            return;
        }
    }
}
