﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day2
{
    internal class Solver : ISolver
    {
        public int DayNumber => 2;

        public string FirstExpected => "6087827";

        public string SecondExpected => "5379";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("1,9,10,3,2,3,11,0,99,30,40,50", 0, 3500L)
                .AddTestCase("1,0,0,0,99", 0, 2)
                .AddTestCase("2,3,0,3,99", 3, 6)
                .AddTestCase("2,4,4,5,99,0", 5, 9801)
                .AddTestCase("1,1,1,4,99,5,6,0,99", 0, 30)
                .AddTestCase("1,1,1,4,99,5,6,0,99", 4, 2)
                .AddTest((s, index) => First(s.Split(','), index))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder().Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var code = input.First().Split(',');
            code[1] = "12";
            code[2] = "2";
            return First(code, 0).ToString();
        }

        private long First(IEnumerable<string> input, int index)
        {
            var factory = new IntCodeFactory();
            var computer = factory.Create(input);
            computer.Execute(new long[0]);

            return computer.GetResult(index);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var raw = input.First().Split(',');
            var target = 19690720;
            for(var noun = 0; noun < 100; noun++)
            {
                for(var verb = 0; verb < 100; verb++)
                {
                    var code = raw.ToArray();
                    code[1] = $"{noun}";
                    code[2] = $"{verb}";
                    var actual = First(code, 0);
                    if (actual == target)
                    {
                        return (100 * noun + verb).ToString();
                    }
                }
            }

            throw new InvalidOperationException("Input cannot be solved");
        }
    }
}
