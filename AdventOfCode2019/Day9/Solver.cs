﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day9
{
    internal class Solver : ISolver
    {
        public int DayNumber => 9;

        public string FirstExpected => "2436480432";

        public string SecondExpected => "45710";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("104,1125899906842624,99", "1125899906842624")
                .AddTestCase("1102,34915192,34915192,7,4,7,99,0", "1219070632396864")
                .AddTestCase("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99", "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99")
                .AddTest(s =>
                {
                    var code = s.Split(',');
                    var factory = new IntCodeFactory();
                    var computer = factory.Create(code);

                    var output = computer.Execute(new long[0]);
                    return string.Join(',', output);
                })
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return RunBoost(input.First(), 1L);
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return RunBoost(input.First(), 2L);
        }

        private static string RunBoost(string rawCode, long input)
        {
            var code = rawCode.Split(',');
            var factory = new IntCodeFactory();
            var computer = factory.Create(code);

            var output = computer.Execute(new[] { input });
            return output.First().ToString();
        }
    }
}
