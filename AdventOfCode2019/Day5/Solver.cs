﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day5
{
    internal class Solver : ISolver
    {
        public int DayNumber => 5;

        public string FirstExpected => "12428642";
        
        public string SecondExpected => "918655";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase("3,0,4,0,99", 75L)
                .AddTest(c => First(c, 75))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                    .AddTestCase("3,9,8,9,10,9,4,9,99,-1,8", 3, 0L)
                    .AddTestCase("3,9,8,9,10,9,4,9,99,-1,8", 8, 1)
                    .AddTestCase("3,9,7,9,10,9,4,9,99,-1,8", 3, 1)
                    .AddTestCase("3,9,7,9,10,9,4,9,99,-1,8", 8, 0)
                    .AddTestCase("3,3,1108,-1,8,3,4,3,99", 3, 0)
                    .AddTestCase("3,3,1108,-1,8,3,4,3,99", 8, 1)
                    .AddTestCase("3,3,1107,-1,8,3,4,3,99", 3, 1)
                    .AddTestCase("3,3,1107,-1,8,3,4,3,99", 8, 0)
                    .AddTestCase("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", 0, 0)
                    .AddTestCase("3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9", 2, 1)
                    .AddTestCase("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", 0, 0)
                    .AddTestCase("3,3,1105,-1,9,1101,0,0,12,4,12,99,1", 2, 1)
                    .AddTestCase("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99", 7, 999)
                    .AddTestCase("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99", 8, 1000)
                    .AddTestCase("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99", 9, 1001)
                    .AddTest((c, i) => First(c, i))
                    .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input.First(), 1).ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return First(input.First(), 5).ToString();
        }

        private long First(string code, long input)
        {
            var factory = new IntCodeFactory();
            var program = factory.Create(code.Split(','));
            var output = program.Execute(new[] { input });

            return output.Last();
        }
    }
}
