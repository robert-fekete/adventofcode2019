﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdventOfCode2019.Day1
{
    internal class SpaceShip
    {
        private readonly IEnumerable<int> modules;

        private SpaceShip(IEnumerable<int> modules)
        {
            this.modules = modules;
        }

        public long GetFuelRequirement(IFuelCalculator calculator)
        {
            return modules.Sum(m => calculator.Calculate(m));
        }

        public static SpaceShip FromInput(IEnumerable<string> input)
        {
            var modules = input.Select(int.Parse);

            return new SpaceShip(modules);
        }
    }
}
