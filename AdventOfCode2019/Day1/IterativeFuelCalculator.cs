﻿namespace AdventOfCode2019.Day1
{
    internal class IterativeFuelCalculator : IFuelCalculator
    {
        private readonly SimpleFuelCalculator calculator;

        public IterativeFuelCalculator(SimpleFuelCalculator calculator)
        {
            this.calculator = calculator;
        }

        public int Calculate(int weight)
        {
            var total = 0;
            var lastIncrement = calculator.Calculate(weight);
            while (lastIncrement > 0)
            {
                total += lastIncrement;
                lastIncrement = calculator.Calculate(lastIncrement);
            }

            return total;
        }
    }
}
