﻿using AdventOfCode.Framework;
using System.Collections.Generic;

namespace AdventOfCode2019.Day1
{
    internal class Solver : ISolver
    {
        public int DayNumber => 1;

        public string FirstExpected => "3184233";

        public string SecondExpected => "4773483";

        public ITestSuite GetFirstTests()
        {
            var calculator = new SimpleFuelCalculator();
            return new TestBuilder()
                .AddTestCase("12", 2L)
                .AddTestCase("14", 2)
                .AddTestCase("1969", 654)
                .AddTestCase("100756", 33583)
                .AddTest(s =>
                {
                    var weight = int.Parse(s);
                    return calculator.Calculate(weight);
                })
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            var calculator = new IterativeFuelCalculator(new SimpleFuelCalculator());
            return new TestBuilder()
                .AddTestCase("12", 2L)
                .AddTestCase("14", 2)
                .AddTestCase("1969", 966)
                .AddTestCase("100756", 50346)
                .AddTest(s =>
                {
                    var weight = int.Parse(s);
                    return calculator.Calculate(weight);
                })
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var spaceShip = SpaceShip.FromInput(input);

            var fuel = spaceShip.GetFuelRequirement(new SimpleFuelCalculator());

            return fuel.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var spaceShip = SpaceShip.FromInput(input);

            var fuel = spaceShip.GetFuelRequirement(new IterativeFuelCalculator(new SimpleFuelCalculator()));

            return fuel.ToString();
        }
    }
}
