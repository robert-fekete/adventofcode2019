﻿namespace AdventOfCode2019.Day1
{
    internal interface IFuelCalculator
    {
        int Calculate(int weight);
    }
}