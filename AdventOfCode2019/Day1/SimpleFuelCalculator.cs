﻿using System;

namespace AdventOfCode2019.Day1
{
    internal class SimpleFuelCalculator : IFuelCalculator
    {
        private const int MINIMUM_FUEL = 0;
        public int Calculate(int weight)
        {
            var fuel = (weight / 3) - 2;
            return Math.Max(fuel, MINIMUM_FUEL);
        }
    }
}
