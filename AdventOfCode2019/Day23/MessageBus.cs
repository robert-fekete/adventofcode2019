﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day23
{
    internal class MessageBus
    {
        private readonly Dictionary<long, List<(long, long)>> memory = new Dictionary<long, List<(long, long)>>();

        public void ProcessMessages(IEnumerable<long> output) 
        {
            var numbers = output.ToArray();
            if (numbers.Length % 3 != 0)
            {
                throw new InvalidOperationException("Something is not right");
            }

            for (int i = 0; i < numbers.Length; i += 3)
            {
                SendMessage(numbers[i], (numbers[i + 1], numbers[i + 2]));
            }
        }

        public void SendMessage(long address, (long, long) message)
        {
            if (memory.ContainsKey(address))
            {
                memory[address].Add(message);
            }
            else
            {
                memory[address] = new List<(long, long)> { message };
            }
        }

        public IEnumerable<(long, long)> ReceiveMessage(long address)
        {
            if (!memory.ContainsKey(address))
            {
                return Enumerable.Empty<(long, long)>();
            }

            var messages = memory[address].ToArray();
            memory[address].Clear();

            return messages;
        }

        public bool IsEmpty()
        {
            return memory.Any() && Enumerable.Range(0, 50).All(i => !memory.ContainsKey(i) || !memory[i].Any());
        }
    }
}
