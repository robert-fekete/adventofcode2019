﻿using AdventOfCode.Framework;
using AdventOfCode2019.IntCode;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day23
{
    internal class Solver : ISolver
    {
        public int DayNumber => 23;

        public string FirstExpected => "23057";

        public string SecondExpected => "15156";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .Build(true);
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            var firstNat = -1L;
            Solve(input, messageBus =>
            {
                var firstMessage = messageBus.ReceiveMessage(255).First();
                firstNat = firstMessage.Item2;

                return true;
            });

            return firstNat.ToString();
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            var lastNat = -1L;
            Solve(input, messageBus =>
            {
                var natMessages = messageBus.ReceiveMessage(255);
                var lastPacket = natMessages.Last();
                messageBus.SendMessage(0, lastPacket);

                if (lastNat == lastPacket.Item2)
                {
                    return true;
                }
                else
                {
                    lastNat = lastPacket.Item2;
                    return false;
                }
            });

            return lastNat.ToString();
        }

        private void Solve(IEnumerable<string> input, Func<MessageBus, bool> callback)
        {
            var factory = new IntCodeFactory();
            var nics = new Dictionary<int, IIntCodeComputer>();
            var messageBus = new MessageBus();

            for (int i = 0; i < 50; i++)
            {
                nics[i] = factory.Create(input.First().Split(','));
                var messages = nics[i].Execute(new long[] { i });
                messageBus.ProcessMessages(messages);
            }

            while (true)
            {
                if (messageBus.IsEmpty())
                {
                    // Idle network
                    if (callback(messageBus))
                    {
                        return;
                    }
                }

                foreach (var kvp in nics)
                {
                    var address = kvp.Key;
                    var nic = kvp.Value;

                    var messages = messageBus.ReceiveMessage(address);
                    if (messages.Any())
                    {
                        foreach ((var x, var y) in messages)
                        {
                            var output = nic.Execute(new long[] { x, y });
                            messageBus.ProcessMessages(output);
                        }
                    }
                    else
                    {
                        var output = nic.Execute(new long[] { -1 });
                        messageBus.ProcessMessages(output);
                    }
                }
            }

            throw new InvalidOperationException("No... No! This shouldn't be possible...");
        }
    }
}
