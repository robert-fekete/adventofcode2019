﻿using AdventOfCode.Framework;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day3
{
    internal class Solver : ISolver
    {
        public int DayNumber => 3;

        public string FirstExpected => "316";

        public string SecondExpected => "16368";

        public ITestSuite GetFirstTests()
        {
            return new TestBuilder()
                .AddTestCase(("R8,U5,L5,D3", "U7,R6,D4,L4"), 6)
                .AddTestCase(("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62, R66, U55, R34, D71, R55, D58, R83"), 159)
                .AddTestCase(("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"), 135)
                .AddTest(i => First(i.Item1, i.Item2))
                .Build();
        }

        public ITestSuite GetSecondTests()
        {
            return new TestBuilder()
                .AddTestCase(("R8,U5,L5,D3", "U7,R6,D4,L4"), 30)
                .AddTestCase(("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62, R66, U55, R34, D71, R55, D58, R83"), 610)
                .AddTestCase(("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"), 410)
                .AddTest(i => Second(i.Item1, i.Item2))
                .Build();
        }

        public string ExecuteFirst(IEnumerable<string> input)
        {
            return First(input.First(), input.Skip(1).First()).ToString();
        }

        private int First(string wireA, string wireB)
        {
            var a = new Wire(ParseDirections(wireA));
            var b = new Wire(ParseDirections(wireB));
            var panel = new Panel(a, b);
            var points = panel.GetIntersections();
            var minDistance = points.Min(p => p.Key.CalculateDistanceFrom(new Point(0, 0)));

            return minDistance;
        }

        private static IEnumerable<string> ParseDirections(string wireA)
        {
            return wireA.Split(',').Select(p => p.Trim());
        }

        public string ExecuteSecond(IEnumerable<string> input)
        {
            return Second(input.First(), input.Skip(1).First()).ToString();
        }

        private int Second(string wireA, string wireB)
        {
            var a = new Wire(ParseDirections(wireA));
            var b = new Wire(ParseDirections(wireB));
            var panel = new Panel(a, b);
            var points = panel.GetIntersections();
            var minDistance = points.Min(p => p.Value);

            return minDistance;
        }
    }
}
