﻿using System;

namespace AdventOfCode2019.Day3
{
    internal class Point
    {
        private int x;
        private int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Point operator+(Point a, Point b)
        {
            return new Point(a.x + b.x, a.y + b.y);
        }

        public int CalculateDistanceFrom(Point point)
        {
            return Math.Abs(point.x - x) + Math.Abs(point.y - y);
        }

        public override bool Equals(object? obj)
        {
            return obj is Point point &&
                   x == point.x &&
                   y == point.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }
    }
}