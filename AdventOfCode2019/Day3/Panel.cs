﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2019.Day3
{
    internal class Panel
    {
        private Wire a;
        private Wire b;

        public Panel(Wire a, Wire b)
        {
            this.a = a;
            this.b = b;
        }

        internal Dictionary<Point, int> GetIntersections()
        {
            var pointsA = a.GetPoints();
            var intersection = new HashSet<Point>(pointsA.Select(kvp => kvp.Key));

            var pointsB = b.GetPoints();
            intersection.IntersectWith(pointsB.Select(kvp => kvp.Key));

            var origo = new Point(0, 0);
            if (intersection.Contains(origo))
            {
                intersection.Remove(new Point(0, 0));
            }

            return intersection.ToDictionary(i => i, i => pointsA[i] + pointsB[i]);
        }
    }
}