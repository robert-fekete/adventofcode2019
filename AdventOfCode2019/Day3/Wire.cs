﻿using System.Collections.Generic;

namespace AdventOfCode2019.Day3
{
    internal class Wire
    {
        private readonly IEnumerable<string> directions;
        private readonly Dictionary<char, Point> deltas = new Dictionary<char, Point>()
        {
            {'U', new Point(0, -1) },
            {'D', new Point(0, 1) },
            {'L', new Point(-1, 0) },
            {'R', new Point(1, 0) },
        };

        public Wire(IEnumerable<string> directions)
        {
            this.directions = directions;
        }

        internal Dictionary<Point, int> GetPoints()
        {
            var currentPoint = new Point(0, 0);
            var currentDistance = 0;
            var visited = new Dictionary<Point, int>();
            visited[currentPoint] = currentDistance;

            foreach (var direction in directions)
            {
                var d = direction[0];
                var distance = int.Parse(direction.Substring(1));
                for(int i = 0; i < distance; i++)
                {
                    currentDistance++;
                    currentPoint += deltas[d];
                    if (!visited.ContainsKey(currentPoint))
                    {
                        visited[currentPoint] = currentDistance;
                    }
                }
            }

            return visited;
        }
    }
}