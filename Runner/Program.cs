﻿using AdventOfCode2019;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new RunnerFactory().Create();

            runner.RunAll();
        }
    }
}
